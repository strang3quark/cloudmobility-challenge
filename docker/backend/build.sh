#!/bin/sh

cd ../../

docker build -t registry.gitlab.com/strang3quark/cloudmobility-challenge:backend-1.0-SNAPSHOT -f ./docker/backend/Dockerfile .
docker push registry.gitlab.com/strang3quark/cloudmobility-challenge:backend-1.0-SNAPSHOT
