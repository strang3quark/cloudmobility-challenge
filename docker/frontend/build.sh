#!/bin/sh

cd ../../

docker build -t registry.gitlab.com/strang3quark/cloudmobility-challenge:frontend-1.0-SNAPSHOT -f ./docker/frontend/Dockerfile .
docker push registry.gitlab.com/strang3quark/cloudmobility-challenge:frontend-1.0-SNAPSHOT
