# Cloud mobility technical challenge

## Technologies used

### Backend

 - Java 8+
 - Spring boot 2.4.5
 - Actuator (exposing app and controller metrics for prometheus)
 - Lombok
 - Liquibase 
 - OpenAPI (OpenAPI generator)
 - Mockito
 - AssertJ
 - Hibernate
 - etc...

### Frontend

 - Angular 11
 - Swagger (swagger-codegen)
 - Clarity design
 - Angular calendar


### Others
 - Keycloak
 - Prometheus
 - Docker and Docker compose
 - Gitlab CI


## Assumptions
 - A doctor cannot:
   - book appointments to another doctor
   - see info about other doctor appointments
   - see info about patient appointments
   - book appointments to a patient
   - cancel appointments for a patient
 - A patient cannot:
   - see appointments of other patients
   - book appointments for other patients
   - book appointments in the past
 - The application should not be tied to a RDMS.
   - solved by using JPA and Liquibase
   
## Users

**WARNING:** the users are only registered in keycloak and not on the application itself. They will get registered
on the first login. I could easily put that in the migration, but that would kind of defeat the purpose of having the
users being automatically registered on the first login.

**TL;DR**: The users only get registered after the first login.

| USERNAME | PASSWORD | ROLE |
|-|-|-|
|doctor_zoidberg|zoidberg|doctor|
|doctor_hartman|hartman|doctor|
|doctor_frankenstein|frankenstein|doctor|
|giggityquagmire|patient|patient|
|peterpumpkineater|patient|patient|
|the_briangriffin|patient|patient|


## Database diagram
You can find the database diagram in the diagram directory.

## Possible improvements

### Frontend

 - Better usage of the angular router;
   - I have used LazyLoaded modules and all but still I am loading some components with *ngIf instead of assigning routes;
 - Responsive design, the calendar could also use the day view on smaller screens;
 - Remove hardcoded strings, I could have used NgxTranslate and have the strings in a json file, that would also help to add multi language support if needed.

### Backend

 - Pagination, I assumed that we would not have to many records so I just skipped that.
 - More comments for Javadoc
 - Better approach for validating appointments (Using JSR-303 instead of having them in the service)
 - Some integration tests


### Organization

I didn't followed any specific method of organization with this challenge, I just tried to go with the flow and have some fun doing it. Some commits may be confusing because of that. Also I didn't use gitflow or something like that, I worked only on the master branch.

### Others

It would be nice to have a **Grafana** dashboard connected to prometheus. I also tought about sending logs to elastic search and display them on **Kibana**. I didn't do it because it takes time and I tought it's a bit out of the scope of this challenge.

## Running the application

### Dockerized version

While in the project folder just run:
`docker-compose up`

This would start the following containers:
 - postgres on localhost:5432
 - keycloak on localhost:9990
 - prometheus on localhost:9000
 - backend on localhost:8080 (downloads from gitlab container registry)
 - frontend on localhost:80 (downloads from gitlab container registry)


### Deploy it somewhere without using docker

The pipeline is building the artifacts and exporting them. You can just go to the latest pipeline and grab them from gitlab.

### Compile and run

If you want to compile the application you need the following dependencies:
 
 - maven
 - java 11
 - node 14
 - npm

For the compilation to happen just run `mvn clean install`. This would do the following:
 - Generate java sources according to the OpenAPI specification on the `specification` directory.
 - Generate angular typescript sources according to the OpenAPI specification on the `specification` directory.
 - Download dependencies for both frontend and backend applications
 - Generate fat jar with the backend on `deployable\backend`
 - Build frontend application to `deployable\appointment-frontend`

After compilation you can deploy the app using the artifacts in the `deployable` directory, beware that you still need (postgres and keycloak), included in `docker-compose-dev.yml`

#### Development

Follow the steps mentioned in **Compile and run**.

Make sure you have `docker` and `docker-compose`

Run `docker-compose -f docker-compose-dev.yml up` to start the application dependencies.

To run the backend head up to the `application` directory and execute `mvn spring-boot:run`

To run the frontend make sure you have `angular-cli` installed, if not just run `npm install -g @angular/cli`. Then head up to the `frontend` directory and run `ng serve`
