package io.cloudmobility.challenge.application.model;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AppointmentEntityTest {

    @Test
    void hasFields() {
        assertThat(AppointmentEntity.class)
                .hasDeclaredFields(
                        AppointmentEntity.FIELD_ID,
                        AppointmentEntity.FIELD_DOCTOR,
                        AppointmentEntity.FIELD_PATIENT,
                        AppointmentEntity.FIELD_DATETIME_START,
                        AppointmentEntity.FIELD_DATETIME_END
                );
    }

}