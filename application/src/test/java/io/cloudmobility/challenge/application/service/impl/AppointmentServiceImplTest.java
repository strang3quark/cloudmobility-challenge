package io.cloudmobility.challenge.application.service.impl;

import io.cloudmobility.challenge.application.core.exception.AppException;
import io.cloudmobility.challenge.application.model.AccountEntity;
import io.cloudmobility.challenge.application.model.AppointmentEntity;
import io.cloudmobility.challenge.application.model.DoctorEntity;
import io.cloudmobility.challenge.application.repository.AppointmentRepository;
import io.cloudmobility.challenge.application.repository.specification.AppointmentSpecification;
import io.cloudmobility.challenge.application.service.AppointmentService;
import io.cloudmobility.challenge.application.service.DoctorService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;


import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AppointmentServiceImplTest {

    private AppointmentService subject;

    @Mock
    private AppointmentRepository appointmentRepository;

    @Mock
    private DoctorService doctorService;

    @BeforeEach
    void setUp() {
        subject = new AppointmentServiceImpl(appointmentRepository, doctorService);
    }

    @Test
    void findAppointmentById() {
        final UUID inputUUID = UUID.randomUUID();
        final Optional<AppointmentEntity> expectedResult = Optional.empty();

        when(appointmentRepository.findById(inputUUID)).thenReturn(expectedResult);

        final var result = subject.findAppointmentById(inputUUID);

        verify(appointmentRepository, times(1)).findById(inputUUID);

        assertThat(result).isEqualTo(expectedResult);
    }


    private static Object[][] findAppointmentsDataProvider() {
        return new Object[][]{
                {null, null, null, null, null, null},
                {UUID.randomUUID(), null, null, null, null, null},
                {UUID.randomUUID(), UUID.randomUUID(), null, null, null, null},
                {UUID.randomUUID(), UUID.randomUUID(), OffsetDateTime.MIN, null, null, null},
                {UUID.randomUUID(), UUID.randomUUID(), OffsetDateTime.MIN, OffsetDateTime.MIN.plusHours(1), null, null},
                {UUID.randomUUID(), UUID.randomUUID(), OffsetDateTime.MIN, OffsetDateTime.MIN.plusHours(1), OffsetDateTime.MAX.minusHours(1), null},
                {UUID.randomUUID(), UUID.randomUUID(), OffsetDateTime.MIN, OffsetDateTime.MIN.plusHours(1), OffsetDateTime.MAX.minusHours(1), OffsetDateTime.MAX},
                {null, UUID.randomUUID(), OffsetDateTime.MIN, OffsetDateTime.MIN.plusHours(1), OffsetDateTime.MAX.minusHours(1), OffsetDateTime.MAX},
                {null, null, OffsetDateTime.MIN, OffsetDateTime.MIN.plusHours(1), OffsetDateTime.MAX.minusHours(1), OffsetDateTime.MAX},
                {null, null, null, OffsetDateTime.MIN.plusHours(1), OffsetDateTime.MAX.minusHours(1), OffsetDateTime.MAX},
                {null, null, null, null, OffsetDateTime.MAX.minusHours(1), OffsetDateTime.MAX},
                {null, null, null, null, null, OffsetDateTime.MAX},
        };
    }

    @ParameterizedTest
    @MethodSource("findAppointmentsDataProvider")
    void findAppointments(
            UUID doctorId, UUID patientId,
            OffsetDateTime startDateFrom, OffsetDateTime startDateTo,
            OffsetDateTime endDateFrom, OffsetDateTime endDateTo) {

        List<AppointmentEntity> expectedResult = List.of(mock(AppointmentEntity.class));

        var sortCaptor = ArgumentCaptor.forClass(Sort.class);

        when(appointmentRepository.findAll(any(Specification.class), sortCaptor.capture())).thenReturn(expectedResult);

        MockedStatic<AppointmentSpecification> specificationMock = mockStatic(AppointmentSpecification.class);

        var result = subject.findAppointments(
                doctorId, patientId, startDateFrom, startDateTo, endDateFrom, endDateTo
        );

        specificationMock.verify(times(1), AppointmentSpecification::joinDoctor);
        specificationMock.verify(times(1), AppointmentSpecification::joinPatient);

        specificationMock.verify(times(doctorId == null ? 0 : 1),
                () -> AppointmentSpecification.filterDoctorId(doctorId));

        specificationMock.verify(times(patientId == null ? 0 : 1),
                () -> AppointmentSpecification.filterPatientId(patientId));

        specificationMock.verify(times(startDateFrom == null ? 0 : 1),
                () -> AppointmentSpecification.filterScheduledDateTimeStartGreaterThan(startDateFrom));

        specificationMock.verify(times(startDateTo == null ? 0 : 1),
                () -> AppointmentSpecification.filterScheduledDateTimeStartLessThan(startDateTo));

        specificationMock.verify(times(endDateFrom == null ? 0 : 1),
                () -> AppointmentSpecification.filterScheduledDateTimeEndGreaterThan(endDateFrom));

        specificationMock.verify(times(endDateTo == null ? 0 : 1),
                () -> AppointmentSpecification.filterScheduledDateTimeEndLessThan(endDateTo));

        specificationMock.close();

        assertThat(result).isEqualTo(expectedResult);
        assertThat(sortCaptor.getValue())
                .isNotNull()
                .isEqualTo(Sort.by(Sort.Direction.ASC, "datetimeStart"));
    }


    @Test
    void createAppointment() {
        AppointmentEntity appointmentMock = mock(AppointmentEntity.class);
        AccountEntity accountEntityMock = mock(AccountEntity.class);
        DoctorEntity doctorMock = mock(DoctorEntity.class);
        UUID doctorMockId = UUID.randomUUID();

        when(appointmentMock.getDoctor()).thenReturn(doctorMock);
        when(appointmentMock.getPatient()).thenReturn(accountEntityMock);
        when(doctorMock.getId()).thenReturn(doctorMockId);
        when(appointmentMock.getDatetimeStart()).thenReturn(offsetDateTimeOf(2500, 10, 10));
        when(appointmentMock.getDatetimeEnd()).thenReturn(offsetDateTimeOf(2500, 10, 11));

        when(doctorMock.getHourStart()).thenReturn(0);
        when(doctorMock.getHourEnd()).thenReturn(23);
        when(doctorService.findDoctorById(doctorMockId)).thenReturn(Optional.of(doctorMock));
        when(appointmentRepository.save(appointmentMock)).thenReturn(appointmentMock);

        final var result = subject.createAppointment(appointmentMock);

        verify(doctorService, times(1)).findDoctorById(doctorMockId);
        verify(appointmentRepository, times(1)).save(appointmentMock);
        verify(appointmentRepository,times(1)).findAll(any(Specification.class), any(Sort.class));

        assertThat(result).isEqualTo(appointmentMock);
    }

    @Test
    void createAppointment_outsideDoctorSchedule_throwException() {
        AppointmentEntity appointmentMock = mock(AppointmentEntity.class);
        DoctorEntity doctorMock = mock(DoctorEntity.class);
        AccountEntity accountEntityMock = mock(AccountEntity.class);
        UUID doctorMockId = UUID.randomUUID();

        when(appointmentMock.getDoctor()).thenReturn(doctorMock);
        when(appointmentMock.getPatient()).thenReturn(accountEntityMock);
        when(doctorMock.getId()).thenReturn(doctorMockId);
        when(appointmentMock.getDatetimeStart()).thenReturn(offsetDateTimeOf(2500, 10, 18));
        when(appointmentMock.getDatetimeEnd()).thenReturn(offsetDateTimeOf(2500, 10, 19));

        when(doctorMock.getHourStart()).thenReturn(12);
        when(doctorMock.getHourEnd()).thenReturn(14);
        when(doctorService.findDoctorById(doctorMockId)).thenReturn(Optional.of(doctorMock));

        assertThatThrownBy(() -> subject.createAppointment(appointmentMock))
                .isInstanceOf(AppException.class)
                .hasMessageContaining("This period is already booked");

        verify(doctorService, times(1)).findDoctorById(doctorMockId);
        verify(appointmentRepository, times(0)).save(appointmentMock);
        verify(appointmentRepository,times(0)).findAll(any(Specification.class), any(Sort.class));
    }

    private static Object[][] findCreateAppointmentCollisionDataProvider() {
        return new Object[][]{
                {offsetDateTimeOf(2500, 10, 14), offsetDateTimeOf(2500, 10, 16)},
                {offsetDateTimeOf(2500, 10, 15), offsetDateTimeOf(2500, 10, 16)},
                {offsetDateTimeOf(2500, 10, 19), offsetDateTimeOf(2500, 10, 21)},
                {offsetDateTimeOf(2500, 10, 14), offsetDateTimeOf(2500, 10, 21)},
        };
    }

    @ParameterizedTest
    @MethodSource("findCreateAppointmentCollisionDataProvider")
    void createAppointment_appointmentCollision_throwException(OffsetDateTime from, OffsetDateTime to) {
        AppointmentEntity existingAppointmentMock = mock(AppointmentEntity.class);
        when(existingAppointmentMock.getDatetimeStart()).thenReturn(from);
        when(existingAppointmentMock.getDatetimeEnd()).thenReturn(to);
        when(appointmentRepository.findAll(any(Specification.class), any(Sort.class))).thenReturn(List.of(existingAppointmentMock));

        AppointmentEntity appointmentMock = mock(AppointmentEntity.class);
        AccountEntity accountEntityMock = mock(AccountEntity.class);
        DoctorEntity doctorMock = mock(DoctorEntity.class);
        UUID doctorMockId = UUID.randomUUID();

        when(appointmentMock.getDoctor()).thenReturn(doctorMock);
        when(appointmentMock.getPatient()).thenReturn(accountEntityMock);
        when(doctorMock.getId()).thenReturn(doctorMockId);
        when(appointmentMock.getDatetimeStart()).thenReturn(offsetDateTimeOf(2500, 10, 15));
        when(appointmentMock.getDatetimeEnd()).thenReturn(offsetDateTimeOf(2500, 10, 20));

        assertThatThrownBy(() -> subject.createAppointment(appointmentMock))
                .isInstanceOf(AppException.class)
                .hasMessageContaining("This period is already booked");

        verify(appointmentRepository,times(1)).findAll(any(Specification.class), any(Sort.class));
    }

    @Test
    void createAppointment_past_doctor() {
        AppointmentEntity appointmentMock = mock(AppointmentEntity.class);
        DoctorEntity doctorMock = mock(DoctorEntity.class);
        AccountEntity accountEntityMock = mock(AccountEntity.class);
        UUID doctorMockId = UUID.randomUUID();

        when(appointmentMock.getDoctor()).thenReturn(doctorMock);
        when(appointmentMock.getPatient()).thenReturn(accountEntityMock);
        when(doctorMock.getId()).thenReturn(doctorMockId);
        when(accountEntityMock.getId()).thenReturn(doctorMockId);
        when(appointmentMock.getDatetimeStart()).thenReturn(offsetDateTimeOf(2000, 10, 18));
        when(appointmentMock.getDatetimeEnd()).thenReturn(offsetDateTimeOf(2000, 10, 19));
        when(appointmentRepository.save(appointmentMock)).thenReturn(appointmentMock);

        final var result = subject.createAppointment(appointmentMock);

        verify(doctorService, times(1)).findDoctorById(doctorMockId);
        verify(appointmentRepository, times(1)).save(appointmentMock);
        verify(appointmentRepository,times(1)).findAll(any(Specification.class), any(Sort.class));

        assertThat(result).isEqualTo(appointmentMock);
    }

    @Test
    void createAppointment_past_patient_throwException() {
        AppointmentEntity appointmentMock = mock(AppointmentEntity.class);
        DoctorEntity doctorMock = mock(DoctorEntity.class);
        AccountEntity accountEntityMock = mock(AccountEntity.class);
        UUID doctorMockId = UUID.randomUUID();

        when(appointmentMock.getDoctor()).thenReturn(doctorMock);
        when(appointmentMock.getPatient()).thenReturn(accountEntityMock);
        when(doctorMock.getId()).thenReturn(doctorMockId);
        when(accountEntityMock.getId()).thenReturn(UUID.randomUUID());
        when(appointmentMock.getDatetimeStart()).thenReturn(offsetDateTimeOf(2000, 10, 18));

        assertThatThrownBy(() -> subject.createAppointment(appointmentMock))
                .isInstanceOf(AppException.class)
                .hasMessageContaining("Cannot book events in the past");

        verify(appointmentRepository, times(0)).save(appointmentMock);
        verify(appointmentRepository,times(0)).findAll(any(Specification.class), any(Sort.class));
    }

    @Test
    void deleteAppointment() {
        AppointmentEntity appointmentMock = mock(AppointmentEntity.class);

        subject.deleteAppointment(appointmentMock);

        verify(appointmentRepository, times(1)).delete(appointmentMock);
    }

    private static OffsetDateTime offsetDateTimeOf(int year, int day, int hour) {
        return OffsetDateTime.of(
                year, 1, day, hour, 0, 0, 0, ZoneOffset.UTC
        );
    }
}