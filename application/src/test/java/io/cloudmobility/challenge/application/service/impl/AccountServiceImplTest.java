package io.cloudmobility.challenge.application.service.impl;

import io.cloudmobility.challenge.application.core.exception.AppException;
import io.cloudmobility.challenge.application.model.AccountEntity;
import io.cloudmobility.challenge.application.repository.AccountRepository;
import io.cloudmobility.challenge.application.service.AccountService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AccountServiceImplTest {

    private AccountService subject;

    @Mock
    private AccountRepository accountRepository;

    @BeforeEach
    void setUp() {
        subject = new AccountServiceImpl(accountRepository);
    }

    @Test
    void findAccountById() {
        final UUID inputUUID = UUID.randomUUID();
        final Optional<AccountEntity> expectedResult = Optional.empty();

        when(accountRepository.findById(inputUUID)).thenReturn(expectedResult);

        final var result = subject.findAccountById(inputUUID);

        verify(accountRepository, times(1)).findById(inputUUID);

        assertThat(result).isEqualTo(expectedResult);
    }

    @Test
    void createAccount() {
        final UUID inputUUID = UUID.randomUUID();
        final AccountEntity inputAccountEntity = mock(AccountEntity.class);
        final Optional<AccountEntity> expectedResult = Optional.empty();

        when(inputAccountEntity.getId()).thenReturn(inputUUID);
        when(accountRepository.findById(inputUUID)).thenReturn(expectedResult);
        when(accountRepository.save(inputAccountEntity)).thenReturn(inputAccountEntity);

        final AccountEntity result = subject.createAccount(inputAccountEntity);

        verify(inputAccountEntity, times(1)).getId();
        verify(accountRepository, times(1)).findById(inputUUID);
        verify(accountRepository, times(1)).save(inputAccountEntity);

        assertThat(result).isEqualTo(inputAccountEntity);
    }

    @Test
    void createAccount_throwAlreadyRegistered() {
        final UUID inputUUID = UUID.randomUUID();
        final AccountEntity inputAccountEntity = mock(AccountEntity.class);
        final Optional<AccountEntity> expectedResult = Optional.of(inputAccountEntity);

        when(inputAccountEntity.getId()).thenReturn(inputUUID);
        when(accountRepository.findById(inputUUID)).thenReturn(expectedResult);

        assertThatThrownBy(() -> subject.createAccount(inputAccountEntity))
                .isInstanceOf(AppException.class);

        verify(inputAccountEntity, times(2)).getId();
        verify(accountRepository, times(1)).findById(inputUUID);
        verify(accountRepository, times(0)).save(inputAccountEntity);

    }
}