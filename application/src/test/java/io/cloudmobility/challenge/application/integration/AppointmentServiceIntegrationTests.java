package io.cloudmobility.challenge.application.integration;

import io.cloudmobility.challenge.application.Application;
import io.cloudmobility.challenge.application.model.AccountEntity;
import io.cloudmobility.challenge.application.model.AppointmentEntity;
import io.cloudmobility.challenge.application.model.DoctorEntity;
import io.cloudmobility.challenge.application.repository.AppointmentRepository;
import io.cloudmobility.challenge.application.service.AccountService;
import io.cloudmobility.challenge.application.service.AppointmentService;
import io.cloudmobility.challenge.application.service.DoctorService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(classes = Application.class)
public class AppointmentServiceIntegrationTests {

    private final AccountService accountService;
    private final DoctorService doctorService;
    private final AppointmentService appointmentService;

    @Autowired
    public AppointmentServiceIntegrationTests(
            AccountService accountService,
            DoctorService doctorService,
            AppointmentService appointmentService) {
        this.accountService = accountService;
        this.doctorService = doctorService;
        this.appointmentService = appointmentService;
    }

    @Test
    public void testAddAndRetrieveData() {
        final var doctorUUID = UUID.randomUUID();

        final var account = AccountEntity.builder().id(doctorUUID)
                .email("mail@mail.com").name("DoctorName").build();

        final var doctor = DoctorEntity.builder().id(doctorUUID)
                .hourStart(3).hourEnd(20).build();

        accountService.createAccount(account);
        doctorService.createDoctor(doctor);

        AppointmentEntity[] appointmentEntities = new AppointmentEntity[]{
                AppointmentEntity.builder()
                        .id(doctorUUID)
                        .doctor(doctor)
                        .patient(account)
                        .datetimeStart(OffsetDateTime.of(2500, 1, 1, 10, 0, 0, 0, ZoneOffset.UTC))
                        .datetimeEnd(OffsetDateTime.of(2500, 1, 1, 11, 0, 0, 0, ZoneOffset.UTC))
                        .build()
        };

        Arrays.stream(appointmentEntities).forEach(appointmentService::createAppointment);

        final var result = appointmentService.findAppointments(doctorUUID, null, null, null, null, null);

        assertThat(result).isNotNull().hasSize(1);
        assertThat(result.get(0).getDoctor()).hasFieldOrPropertyWithValue("id", doctorUUID);
        assertThat(result.get(0).getPatient()).hasFieldOrPropertyWithValue("id", doctorUUID);
        assertThat(result.get(0).getId()).isNotNull();
    }
}
