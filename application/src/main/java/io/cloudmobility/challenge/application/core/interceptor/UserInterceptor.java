package io.cloudmobility.challenge.application.core.interceptor;

import io.cloudmobility.challenge.application.configuration.HospitalConfiguration;
import io.cloudmobility.challenge.application.core.exception.AppException;
import io.cloudmobility.challenge.application.core.exception.AppExceptionEnum;
import io.cloudmobility.challenge.application.model.AccountEntity;
import io.cloudmobility.challenge.application.model.DoctorEntity;
import io.cloudmobility.challenge.application.model.RoleEnum;
import io.cloudmobility.challenge.application.service.AccountService;
import io.cloudmobility.challenge.application.service.DoctorService;
import io.cloudmobility.challenge.application.service.impl.AccountContextServiceImpl;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * This interceptor gathers the token information and registers the account if necessary
 * No updates or deletions are triggered via this interceptor, it only triggers creation of new records
 */
@Component
@Log4j2
public class UserInterceptor implements HandlerInterceptor {

    private final HospitalConfiguration hospitalConfiguration;
    private final AccountContextServiceImpl accountContextService;
    private final AccountService accountService;
    private final DoctorService doctorService;

    @Autowired
    public UserInterceptor(HospitalConfiguration hospitalConfiguration, AccountContextServiceImpl accountContextService,
                           AccountService accountService,
                           DoctorService doctorService) {

        this.hospitalConfiguration = hospitalConfiguration;
        this.accountContextService = accountContextService;
        this.accountService = accountService;
        this.doctorService = doctorService;
    }

    @Override
    public boolean preHandle(
            @NonNull HttpServletRequest request, @NonNull HttpServletResponse response, @NonNull Object handler) {

        final UUID userId = accountContextService.fetchUserId();
        final String name = accountContextService.fetchName();
        final String email = accountContextService.fetchEmail();
        final Set<RoleEnum> roles = accountContextService.fetchRoles();

        if (CollectionUtils.isEmpty(roles)) {
            log.error("User has no valid role: {}", userId);
            throw new AppException(AppExceptionEnum.NO_PERMISSIONS, String.format("No roles for %s", userId), null);
        }

        final AccountEntity accountEntity = accountService.findAccountById(userId)
                .orElseGet(() -> accountService.createAccount(
                        AccountEntity.builder()
                                .id(userId).email(email).name(name)
                                .build()
                        )
                );

        log.debug("Request from {}", accountEntity.getId());

        if (roles.contains(RoleEnum.DOCTOR)) {
            final DoctorEntity doctor = doctorService.findDoctorById(accountEntity.getId())
                    .orElseGet(() -> doctorService.createDoctor(
                            DoctorEntity.builder()
                                    .id(accountEntity.getId())
                                    .hourStart(hospitalConfiguration.getHourOpen())
                                    .hourEnd(hospitalConfiguration.getHourClose())
                                    .build()
                            )
                    );

            log.debug("{} is a doctor", doctor.getId());
        }

        return true;
    }
}
