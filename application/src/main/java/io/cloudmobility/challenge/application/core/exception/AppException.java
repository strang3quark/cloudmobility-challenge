package io.cloudmobility.challenge.application.core.exception;

/**
 * Default exception for the AppointmentApp
 */
public class AppException extends RuntimeException {

    private final AppExceptionEnum exceptionEnum;

    public AppException(AppExceptionEnum exceptionEnum) {
        super();
        this.exceptionEnum = exceptionEnum;
    }

    public AppException(AppExceptionEnum exceptionEnum, String message, Throwable cause) {
        super(message, cause);

        this.exceptionEnum = exceptionEnum;
    }

    public AppExceptionEnum getExceptionEnum() {
        return exceptionEnum;
    }
}
