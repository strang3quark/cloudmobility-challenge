package io.cloudmobility.challenge.application.controller;

import io.cloudmobility.challenge.application.facade.PatientFacade;
import io.cloudmobility.challenge.codegen.dto.AppointmentWithDoctorDTO;
import io.cloudmobility.challenge.codegen.patient.api.PatientApi;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Timed(value = "patient-controller")
@RestController
public class PatientController implements PatientApi {

    private final PatientFacade patientFacade;

    @Autowired
    public PatientController(PatientFacade patientFacade) {
        this.patientFacade = patientFacade;
    }

    @Override
    public ResponseEntity<Void> createAppointment(AppointmentWithDoctorDTO appointmentWithDoctorDTO) {
        patientFacade.createAppointment(appointmentWithDoctorDTO);
        return ResponseEntity.status(HttpStatus.CREATED.value()).build();
    }

    @Override
    public ResponseEntity<Void> deleteAppointment(UUID id) {
        patientFacade.deleteAppointment(id);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<List<AppointmentWithDoctorDTO>> getPatientAppointments(OffsetDateTime from, OffsetDateTime to) {
        final List<AppointmentWithDoctorDTO> resultBody = patientFacade.getPatientAppointments(from, to);
        return ResponseEntity.ok(resultBody);
    }
}