package io.cloudmobility.challenge.application.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "hospital")
public class HospitalConfiguration {

    private Integer hourOpen;
    private Integer hourClose;
}
