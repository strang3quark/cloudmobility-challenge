package io.cloudmobility.challenge.application.model;

import java.util.Arrays;
import java.util.Optional;

public enum RoleEnum {
    DOCTOR,
    PATIENT;

    public static Optional<RoleEnum> fromString(String value) {
        return Arrays.stream(RoleEnum.values())
                .filter(enumValue -> enumValue.toString().equals(value))
                .findFirst();
    }

}
