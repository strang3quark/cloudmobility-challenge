package io.cloudmobility.challenge.application.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@Entity
@Table(name = "doctor")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DoctorEntity {

    public static final String FIELD_ID = "id";

    @Id
    @Column(name = "id")
    private UUID id;

    @NotNull
    @Size(max = 23)
    @Column(name = "hour_start")
    private Integer hourStart;

    @NotNull
    @Size(max = 59)
    @Column(name = "hour_end")
    private Integer hourEnd;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    private AccountEntity account;

}
