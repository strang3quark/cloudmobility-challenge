package io.cloudmobility.challenge.application.core.exception;


import org.springframework.http.HttpStatus;

/**
 * Causes for the AppException
 */
public enum AppExceptionEnum {

    UNKNOWN(0, HttpStatus.INTERNAL_SERVER_ERROR),
    NOT_FOUND(1, HttpStatus.NOT_FOUND),
    NO_PERMISSIONS(2, HttpStatus.UNAUTHORIZED),
    ALREADY_BOOKED(3, HttpStatus.CONFLICT),
    ILLEGAL_OPERATION(4, HttpStatus.BAD_REQUEST),
    ALREADY_REGISTERED(5, HttpStatus.CONFLICT),
    MISSING_INFORMATION(6, HttpStatus.BAD_REQUEST);

    private final int id;
    private final HttpStatus httpStatus;

    AppExceptionEnum(int id, HttpStatus httpStatus) {
        this.id = id;
        this.httpStatus = httpStatus;
    }

    public int getId() {
        return id;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}