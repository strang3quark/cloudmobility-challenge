package io.cloudmobility.challenge.application.service.impl;

import io.cloudmobility.challenge.application.model.RoleEnum;
import io.cloudmobility.challenge.application.service.AccountContextService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class AccountContextServiceImpl implements AccountContextService {

    private static final String SCOPE_SEPARATOR = "\\s";

    @Override
    public UUID fetchUserId() {
        return UUID.fromString(getClaim("sub"));
    }

    @Override
    public String fetchName() {
        return getClaim("name");
    }

    @Override
    public String fetchEmail() {
        return getClaim("email");
    }

    @Override
    public String[] fetchScopes() {
        final String scopeString = getClaim("scope");

        if (StringUtils.hasText(scopeString)) {
            return scopeString.split(SCOPE_SEPARATOR);
        }

        return new String[0];
    }

    @Override
    public Set<RoleEnum> fetchRoles() {
        return Arrays.stream(fetchScopes())
                .map(String::toUpperCase)
                .map(RoleEnum::fromString)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toSet());
    }

    private static String getClaim(String key) {
        return (String) getClaims().get(key);
    }

    private static Map<String, Object> getClaims() {
        if (SecurityContextHolder.getContext() != null
                && SecurityContextHolder.getContext().getAuthentication() != null
                && SecurityContextHolder.getContext().getAuthentication().getCredentials() != null
        ) {
            Jwt credentials = (Jwt) SecurityContextHolder.getContext().getAuthentication().getCredentials();
            return credentials.getClaims();
        }

        return Map.of();
    }
}
