package io.cloudmobility.challenge.application.service;

import io.cloudmobility.challenge.application.model.AppointmentEntity;

import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface AppointmentService {

    Optional<AppointmentEntity> findAppointmentById(@NotNull UUID id);

    List<AppointmentEntity> findAppointments(
            UUID doctorId, UUID patientId, OffsetDateTime startDateFrom, OffsetDateTime startDateTo,
            OffsetDateTime endDateFrom, OffsetDateTime endDateTo);

    AppointmentEntity createAppointment(AppointmentEntity appointmentEntity);

    void deleteAppointment(AppointmentEntity appointmentEntity);
}
