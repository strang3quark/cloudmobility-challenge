package io.cloudmobility.challenge.application.service;

import io.cloudmobility.challenge.application.model.AccountEntity;

import javax.validation.constraints.NotNull;
import java.util.Optional;
import java.util.UUID;

public interface AccountService {

    Optional<AccountEntity> findAccountById(@NotNull UUID id);

    AccountEntity createAccount(AccountEntity accountEntity);
}
