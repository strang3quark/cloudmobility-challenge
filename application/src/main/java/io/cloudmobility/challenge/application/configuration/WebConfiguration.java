package io.cloudmobility.challenge.application.configuration;

import io.cloudmobility.challenge.application.core.interceptor.UserInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.format.datetime.standard.DateTimeFormatterRegistrar;
import org.springframework.lang.NonNull;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Web related configuration
 *
 *  - Allow CORS
 *  - Register Interceptors
 *  - Secure endpoints
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebConfiguration {

    @Bean
    public WebMvcConfigurer webMvcConfigurer(UserInterceptor userInterceptor) {
        return new WebMvcConfigurer() {

            @Override
            public void addInterceptors(@NonNull InterceptorRegistry registry) {
                registry.addInterceptor(userInterceptor);
            }

            @Override
            public void addCorsMappings(@NonNull CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedHeaders("*")
                        .allowedMethods("*")
                        .allowedOrigins("*")
                        .exposedHeaders("Location");
            }

            @Override
            public void addFormatters(@NonNull FormatterRegistry registry) {
                var registrar = new DateTimeFormatterRegistrar();
                registrar.setUseIsoFormat(true);
                registrar.registerFormatters(registry);
            }
        };
    }

    @Bean
    public WebSecurityConfigurerAdapter webSecurityConfigurerAdapter() {
        return new WebSecurityConfigurerAdapter() {
            @Override
            protected void configure(HttpSecurity http) throws Exception {
                http.cors().and()
                        .authorizeRequests().antMatchers("/actuator/**").permitAll()
                        .antMatchers("/**")
                        .hasAnyAuthority("SCOPE_patient", "SCOPE_doctor")
                        .anyRequest().denyAll()
                        .and()
                        .oauth2ResourceServer()
                        .jwt();
            }
        };
    }
}
