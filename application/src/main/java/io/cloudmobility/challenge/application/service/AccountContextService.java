package io.cloudmobility.challenge.application.service;

import io.cloudmobility.challenge.application.model.RoleEnum;

import java.util.Set;
import java.util.UUID;

public interface AccountContextService {

    UUID fetchUserId();

    String fetchName();

    String fetchEmail();

    String[] fetchScopes();

    Set<RoleEnum> fetchRoles();
}
