package io.cloudmobility.challenge.application.service.impl;

import io.cloudmobility.challenge.application.core.exception.AppException;
import io.cloudmobility.challenge.application.core.exception.AppExceptionEnum;
import io.cloudmobility.challenge.application.model.AccountEntity;
import io.cloudmobility.challenge.application.repository.AccountRepository;
import io.cloudmobility.challenge.application.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.UUID;

@Service
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    @Autowired
    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<AccountEntity> findAccountById(UUID id) {
        return accountRepository.findById(id);
    }

    @Override
    @Transactional
    public AccountEntity createAccount(AccountEntity accountEntity) {
        findAccountById(accountEntity.getId()).ifPresent(
                account -> {
                    throw new AppException(
                            AppExceptionEnum.ALREADY_REGISTERED,
                            String.format("This user is already registered: %s", accountEntity.getId()),
                            null);
                }
        );
        return accountRepository.save(accountEntity);
    }
}
