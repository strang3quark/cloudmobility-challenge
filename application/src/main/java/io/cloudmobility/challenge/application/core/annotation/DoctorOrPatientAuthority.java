package io.cloudmobility.challenge.application.core.annotation;

import org.springframework.security.access.prepost.PreAuthorize;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Alias to @PreAuthorize("hasAnyAuthority(\"SCOPE_doctor\", \"SCOPE_patient\")")
 *
 * Ensures that only doctors and patients can access a method annotated with this
 */
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@PreAuthorize("hasAnyAuthority(\"SCOPE_doctor\", \"SCOPE_patient\")")
public @interface DoctorOrPatientAuthority {
}
