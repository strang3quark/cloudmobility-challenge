package io.cloudmobility.challenge.application.converter;

import io.cloudmobility.challenge.application.model.DoctorEntity;
import io.cloudmobility.challenge.codegen.dto.DoctorDTO;
import org.modelmapper.AbstractConverter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Converts {@link DoctorEntity} into a {@link DoctorDTO} using {@link ModelMapper}
 */
@Component
public class DoctorToDoctorDTOConverter extends AbstractConverter<DoctorEntity, DoctorDTO> {

    @Autowired
    public DoctorToDoctorDTOConverter(ModelMapper modelMapper) {
        modelMapper.createTypeMap(DoctorEntity.class, DoctorDTO.class)
                .setConverter(this);
    }

    @Override
    protected DoctorDTO convert(DoctorEntity doctorEntity) {
        return new DoctorDTO()
                .id(doctorEntity.getId())
                .hourStart(doctorEntity.getHourStart())
                .hourEnd(doctorEntity.getHourEnd())
                .name(doctorEntity.getAccount().getName())
                .email(doctorEntity.getAccount().getName());
    }
}
