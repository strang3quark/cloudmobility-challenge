package io.cloudmobility.challenge.application.facade.impl;

import io.cloudmobility.challenge.application.core.annotation.DoctorAuthority;
import io.cloudmobility.challenge.application.core.annotation.DoctorOrPatientAuthority;
import io.cloudmobility.challenge.application.core.exception.AppException;
import io.cloudmobility.challenge.application.core.exception.AppExceptionEnum;
import io.cloudmobility.challenge.application.facade.DoctorFacade;
import io.cloudmobility.challenge.application.model.AccountEntity;
import io.cloudmobility.challenge.application.model.AppointmentEntity;
import io.cloudmobility.challenge.application.model.DoctorEntity;
import io.cloudmobility.challenge.application.service.AccountContextService;
import io.cloudmobility.challenge.application.service.AppointmentService;
import io.cloudmobility.challenge.application.service.DoctorService;
import io.cloudmobility.challenge.codegen.dto.AppointmentMinimalDTO;
import io.cloudmobility.challenge.codegen.dto.AppointmentWithPatientDTO;
import io.cloudmobility.challenge.codegen.dto.DoctorDTO;
import io.cloudmobility.challenge.codegen.dto.DoctorScheduleDTO;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Log4j2
@Component
public class DoctorFacadeImpl implements DoctorFacade {

    private final ModelMapper modelMapper;
    private final AccountContextService accountContextService;
    private final AppointmentService appointmentService;
    private final DoctorService doctorService;

    @Autowired
    public DoctorFacadeImpl(ModelMapper modelMapper, AccountContextService accountContextService, AppointmentService appointmentService, DoctorService doctorService) {
        this.modelMapper = modelMapper;
        this.accountContextService = accountContextService;
        this.appointmentService = appointmentService;
        this.doctorService = doctorService;
    }

    @Override
    @DoctorAuthority
    public void createUnavailable(AppointmentMinimalDTO appointmentMinimalDTO) {
        var appointmentEntity = modelMapper.map(appointmentMinimalDTO, AppointmentEntity.class);

        final UUID uuid = accountContextService.fetchUserId();
        appointmentEntity.setDoctor(DoctorEntity.builder().id(uuid).build());
        appointmentEntity.setPatient(AccountEntity.builder().id(uuid).build());

        appointmentService.createAppointment(appointmentEntity);
    }

    @Override
    @DoctorAuthority
    public void deleteUnavailable(UUID id) {
        appointmentService.findAppointmentById(id)
                .filter(doctorUnavailableEntity ->
                        ObjectUtils.nullSafeEquals(
                                doctorUnavailableEntity.getDoctor().getId(),
                                accountContextService.fetchUserId()
                        )
                )
                .ifPresentOrElse(appointmentService::deleteAppointment,
                        () -> {
                            log.error("Cannot delete, Appointment does not exist: {}", id.toString());
                            throw new AppException(AppExceptionEnum.NOT_FOUND, String.format("Cannot delete %s", id), null);
                        });
    }

    @Override
    @DoctorOrPatientAuthority
    public List<AppointmentMinimalDTO> getDoctorAppointments(UUID id, OffsetDateTime from, OffsetDateTime to) {
        return appointmentService.findAppointments(id, null, from, to, null, null)
                .stream()
                .map(e -> modelMapper.map(e, AppointmentMinimalDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    @DoctorAuthority
    public List<AppointmentWithPatientDTO> getDoctorOwnAppointments(OffsetDateTime from, OffsetDateTime to) {
        final UUID userId = accountContextService.fetchUserId();
        return appointmentService.findAppointments(userId, null, from, to, null, null)
                .stream()
                .map(e -> modelMapper.map(e, AppointmentWithPatientDTO.class))
                .collect(Collectors.toList());

    }

    @Override
    @DoctorOrPatientAuthority
    public DoctorDTO getDoctorById(UUID id) {
        return doctorService.findDoctorById(id)
                .map(e -> modelMapper.map(e, DoctorDTO.class))
                .orElseThrow(() -> new AppException(
                                AppExceptionEnum.NOT_FOUND,
                                String.format("Doctor %s does not exist", id),
                                null
                        )
                );
    }

    @Override
    @DoctorOrPatientAuthority
    public List<DoctorDTO> getDoctors() {
        return doctorService.findAllDoctors()
                .stream().map(e -> modelMapper.map(e, DoctorDTO.class))
                .collect(Collectors.toList());
    }

    @Override
    @DoctorAuthority
    public void updateSchedule(DoctorScheduleDTO doctorScheduleDTO) {
        final UUID userId = accountContextService.fetchUserId();

        appointmentService.findAppointments(userId, null,
                OffsetDateTime.now(), null, null, null)
                .stream()
                .filter(ap ->
                        ap.getDatetimeStart().getHour() < doctorScheduleDTO.getHourStart() ||
                                ap.getDatetimeEnd().getHour() > doctorScheduleDTO.getHourEnd()
                )
                .findAny()
                .ifPresent((appointmentEntity) -> {
                    throw new AppException(
                            AppExceptionEnum.ALREADY_BOOKED,
                            "There are appointments booked outside this schedule", null);
                });


        doctorService.updateDoctorSchedule(userId, doctorScheduleDTO.getHourStart(), doctorScheduleDTO.getHourEnd());
    }
}
