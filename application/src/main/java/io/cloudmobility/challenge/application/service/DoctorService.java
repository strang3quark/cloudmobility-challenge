package io.cloudmobility.challenge.application.service;

import io.cloudmobility.challenge.application.model.DoctorEntity;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface DoctorService {

    Optional<DoctorEntity> findDoctorById(@NotNull UUID id);

    List<DoctorEntity> findAllDoctors();

    DoctorEntity createDoctor(@NotNull DoctorEntity accountEntity);

    DoctorEntity updateDoctorSchedule(@NotNull UUID doctorId, int hourStart, int hourEnd);
}
