package io.cloudmobility.challenge.application.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.UUID;

@Entity
@Table(name = "account")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountEntity {

    public static final String FIELD_ID = "id";

    @Id
    @Column(name = "id")
    private UUID id;

    @NotNull
    @Column(name = "email")
    @Size(max = 255)
    private String email;

    @NotNull
    @Column(name = "name")
    @Size(max = 255)
    private String name;
}
