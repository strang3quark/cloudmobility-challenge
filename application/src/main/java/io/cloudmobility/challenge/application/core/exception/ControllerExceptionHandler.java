package io.cloudmobility.challenge.application.core.exception;

import io.cloudmobility.challenge.codegen.dto.ErrorDTO;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.NonNull;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    // 400

    @Override
    @NonNull
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            final MethodArgumentNotValidException ex, @NonNull final HttpHeaders headers,
            @NonNull final HttpStatus status, @NonNull final WebRequest request) {
        final List<String> errors = new ArrayList<>();
        for (final FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (final ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }

        final var errorDTO = new ErrorDTO().id(HttpStatus.BAD_REQUEST.value())
                .id(AppExceptionEnum.UNKNOWN.getId())
                .errorType(AppExceptionEnum.UNKNOWN.name())
                .httpStatus(HttpStatus.BAD_REQUEST.value())
                .message(ex.getLocalizedMessage() + " - " + errors);

        return new ResponseEntity<>(errorDTO, HttpStatus.valueOf(errorDTO.getHttpStatus()));
    }

    @Override
    @NonNull
    protected ResponseEntity<Object> handleBindException(
            final BindException ex, @NonNull final HttpHeaders headers,
            @NonNull final HttpStatus status, @NonNull final WebRequest request) {
        final List<String> errors = new ArrayList<>();
        for (final FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (final ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }

        final var errorDTO = new ErrorDTO()
                .id(AppExceptionEnum.UNKNOWN.getId())
                .errorType(AppExceptionEnum.UNKNOWN.name())
                .httpStatus(HttpStatus.BAD_REQUEST.value())
                .message(ex.getLocalizedMessage() + " - " + errors);

        return new ResponseEntity<>(errorDTO, HttpStatus.valueOf(errorDTO.getHttpStatus()));
    }

    @Override
    @NonNull
    protected ResponseEntity<Object> handleTypeMismatch(
            final TypeMismatchException ex, @NonNull final HttpHeaders headers,
            @NonNull final HttpStatus status, @NonNull final WebRequest request) {
        final String error = ex.getValue() + " value for " + ex.getPropertyName() + " should be of type "
                + ex.getRequiredType();

        final var errorDTO = new ErrorDTO()
                .id(AppExceptionEnum.UNKNOWN.getId())
                .errorType(AppExceptionEnum.UNKNOWN.name())
                .httpStatus(HttpStatus.BAD_REQUEST.value())
                .message(ex.getLocalizedMessage() + " - " + error);

        return new ResponseEntity<>(errorDTO, HttpStatus.valueOf(errorDTO.getHttpStatus()));
    }

    @Override
    @NonNull
    protected ResponseEntity<Object> handleMissingServletRequestPart(
            final MissingServletRequestPartException ex,
            @NonNull final HttpHeaders headers, @NonNull final HttpStatus status,
            @NonNull final WebRequest request) {
        final String error = ex.getRequestPartName() + " part is missing";
        final var errorDTO = new ErrorDTO()
                .id(AppExceptionEnum.UNKNOWN.getId())
                .errorType(AppExceptionEnum.UNKNOWN.name())
                .httpStatus(HttpStatus.BAD_REQUEST.value())
                .message(ex.getLocalizedMessage() + " - " + error);
        return new ResponseEntity<>(errorDTO, HttpStatus.valueOf(errorDTO.getHttpStatus()));
    }

    @Override
    @NonNull
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            final MissingServletRequestParameterException ex,
            @NonNull final HttpHeaders headers, @NonNull final HttpStatus status,
            @NonNull final WebRequest request) {
        final String error = ex.getParameterName() + " parameter is missing";
        final var errorDTO = new ErrorDTO()
                .id(AppExceptionEnum.UNKNOWN.getId())
                .errorType(AppExceptionEnum.UNKNOWN.name())
                .httpStatus(HttpStatus.BAD_REQUEST.value())
                .message(ex.getLocalizedMessage() + " - " + error);
        return new ResponseEntity<>(errorDTO, HttpStatus.valueOf(errorDTO.getHttpStatus()));
    }

    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    public ResponseEntity<Object> handleMethodArgumentTypeMismatch(final MethodArgumentTypeMismatchException ex,
                                                                   final WebRequest request) {
        final var errorDTO = new ErrorDTO()
                .id(AppExceptionEnum.UNKNOWN.getId())
                .errorType(AppExceptionEnum.UNKNOWN.name())
                .httpStatus(HttpStatus.BAD_REQUEST.value())
                .message(ex.getLocalizedMessage());
        return new ResponseEntity<>(errorDTO, HttpStatus.valueOf(errorDTO.getHttpStatus()));
    }

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<Object> handleConstraintViolation(final ConstraintViolationException ex,
                                                            final WebRequest request) {
        final List<String> errors = new ArrayList<>();
        for (final ConstraintViolation<?> violation : ex.getConstraintViolations()) {
            errors.add(violation.getRootBeanClass().getName() + " " + violation.getPropertyPath() + ": "
                    + violation.getMessage());
        }

        final var errorDTO = new ErrorDTO()
                .id(AppExceptionEnum.UNKNOWN.getId())
                .errorType(AppExceptionEnum.UNKNOWN.name())
                .httpStatus(HttpStatus.BAD_REQUEST.value())
                .message(ex.getLocalizedMessage() + " - " + errors);
        return new ResponseEntity<>(errorDTO, HttpStatus.valueOf(errorDTO.getHttpStatus()));
    }

    // 404

    @Override
    @NonNull
    protected ResponseEntity<Object> handleNoHandlerFoundException(
            final NoHandlerFoundException ex, @NonNull final HttpHeaders headers,
            @NonNull final HttpStatus status, @NonNull final WebRequest request) {
        final String error = "No handler found for " + ex.getHttpMethod() + " " + ex.getRequestURL();

        final var errorDTO = new ErrorDTO()
                .id(AppExceptionEnum.UNKNOWN.getId())
                .errorType(AppExceptionEnum.UNKNOWN.name())
                .httpStatus(HttpStatus.NOT_FOUND.value())
                .message(ex.getLocalizedMessage() + " - " + error);
        return new ResponseEntity<>(errorDTO, HttpStatus.valueOf(errorDTO.getHttpStatus()));
    }

    // 405

    @Override
    @NonNull
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
            final HttpRequestMethodNotSupportedException ex, @NonNull final HttpHeaders headers,
            @NonNull final HttpStatus status, @NonNull final WebRequest request) {
        final var builder = new StringBuilder();
        builder.append(ex.getMethod());
        builder.append(" method is not supported for this request.");

        final var errorDTO = new ErrorDTO()
                .id(AppExceptionEnum.UNKNOWN.getId())
                .errorType(AppExceptionEnum.UNKNOWN.name())
                .httpStatus(HttpStatus.METHOD_NOT_ALLOWED.value())
                .message(ex.getLocalizedMessage() + " - " + builder.substring(0, builder.length() - 1));
        return new ResponseEntity<>(errorDTO, HttpStatus.valueOf(errorDTO.getHttpStatus()));
    }

    // 415

    @Override
    @NonNull
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
            final HttpMediaTypeNotSupportedException ex, @NonNull final HttpHeaders headers,
            @NonNull final HttpStatus status, @NonNull final WebRequest request) {
        final var builder = new StringBuilder();
        builder.append(ex.getContentType());
        builder.append(" media type is not supported. Supported media types are ");
        ex.getSupportedMediaTypes().forEach(t -> builder.append(t).append(" "));

        final var errorDTO = new ErrorDTO()
                .id(AppExceptionEnum.UNKNOWN.getId())
                .errorType(AppExceptionEnum.UNKNOWN.name())
                .httpStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE.value())
                .message(ex.getLocalizedMessage() + " - " + builder.substring(0, builder.length() - 1));
        return new ResponseEntity<>(errorDTO, HttpStatus.valueOf(errorDTO.getHttpStatus()));
    }

    // 500

    @ExceptionHandler({Exception.class})
    public ResponseEntity<Object> handleAll(final Exception ex, final WebRequest request) {
        final var errorDTO = new ErrorDTO()
                .id(AppExceptionEnum.UNKNOWN.getId())
                .errorType(AppExceptionEnum.UNKNOWN.name())
                .httpStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
                .message(ex.getLocalizedMessage());
        return new ResponseEntity<>(errorDTO, HttpStatus.valueOf(errorDTO.getHttpStatus()));
    }


    // Custom
    @ExceptionHandler({AppException.class})
    protected ResponseEntity<Object> handleAppException(final AppException ex) {
        final var errorDTO = new ErrorDTO()
                .id(ex.getExceptionEnum().getId())
                .errorType(ex.getExceptionEnum().name())
                .httpStatus(ex.getExceptionEnum().getHttpStatus().value())
                .extra(ex.getMessage())
                .message(ex.getLocalizedMessage());
        return new ResponseEntity<>(errorDTO, ex.getExceptionEnum().getHttpStatus());
    }
}
