package io.cloudmobility.challenge.application.service.impl;

import io.cloudmobility.challenge.application.core.exception.AppException;
import io.cloudmobility.challenge.application.core.exception.AppExceptionEnum;
import io.cloudmobility.challenge.application.model.DoctorEntity;
import io.cloudmobility.challenge.application.repository.DoctorRepository;
import io.cloudmobility.challenge.application.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class DoctorServiceImpl implements DoctorService {

    private final DoctorRepository doctorRepository;

    @Autowired
    public DoctorServiceImpl(DoctorRepository doctorRepository) {
        this.doctorRepository = doctorRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<DoctorEntity> findDoctorById(UUID id) {
        return doctorRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DoctorEntity> findAllDoctors() {
        return doctorRepository.findAll();
    }

    @Override
    @Transactional
    public DoctorEntity createDoctor(DoctorEntity doctorEntity) {
        return doctorRepository.save(doctorEntity);
    }

    @Override
    @Transactional
    public DoctorEntity updateDoctorSchedule(UUID doctorId, int hourStart, int hourEnd) {
        final DoctorEntity doctorEntity = findDoctorById(doctorId)
                .orElseThrow(() -> new AppException(
                        AppExceptionEnum.NOT_FOUND,
                        String.format("Doctor not found: %s", doctorId),
                        null)
                );

        doctorEntity.setHourStart(hourStart);
        doctorEntity.setHourEnd(hourEnd);

        return doctorRepository.save(doctorEntity);
    }
}
