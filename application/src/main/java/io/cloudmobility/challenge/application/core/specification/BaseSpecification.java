package io.cloudmobility.challenge.application.core.specification;

import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;

/**
 * Provides common methods for generating {@link Specification}s
 */
public abstract class BaseSpecification {

    protected BaseSpecification() {
    }

    protected static boolean isCount(Class<?> clazz, CriteriaQuery<?> query) {
        return !isNotCount(clazz, query);
    }

    protected static boolean isNotCount(Class<?> clazz, CriteriaQuery<?> query) {
        return clazz.equals(query.getResultType());
    }

    protected static <T> Specification<T> join(Class<?> clazz, String field, JoinType type) {

        return (root, criteriaQuery, criteriaBuilder) -> {
            if (isNotCount(clazz, criteriaQuery)) {
                root.fetch(field, type);
            }
            return null;
        };
    }
}
