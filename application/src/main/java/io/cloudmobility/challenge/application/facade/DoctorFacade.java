package io.cloudmobility.challenge.application.facade;

import io.cloudmobility.challenge.codegen.dto.AppointmentMinimalDTO;
import io.cloudmobility.challenge.codegen.dto.AppointmentWithPatientDTO;
import io.cloudmobility.challenge.codegen.dto.DoctorDTO;
import io.cloudmobility.challenge.codegen.dto.DoctorScheduleDTO;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

/**
 * Facade for Doctor actions
 */
public interface DoctorFacade {

    /**
     * Create an unavailable slot for the logged in doctor
     * Technically a unavailable slot is an appointment with himself
     *
     * @param appointmentMinimalDTO the slot properties
     */
    void createUnavailable(AppointmentMinimalDTO appointmentMinimalDTO);

    /**
     * Free an unavailable slot for the logged in doctor
     *
     * @param id the appointment id
     */
    void deleteUnavailable(UUID id);

    /**
     * Get the appointments for the specified doctor in the given time interval
     * The appointments returned don't contain any info about the doctor and the patient, not even their ids.
     *
     * @param id   the id of the doctor to look for
     * @param from filter appointments from this date on
     * @param to   filter appointments until this date
     * @return a list with all the appointments that match the given criteria
     */
    List<AppointmentMinimalDTO> getDoctorAppointments(UUID id, OffsetDateTime from, OffsetDateTime to);

    /**
     * Get the appointments for the logged in doctor in the given time interval
     * The appointments returned contain information about the patient
     *
     * @param from filter appointments from this date on
     * @param to   filter appointments until this date
     *
     * @return a list with all the appointments of the logged in doctor (with patient information)
     * that match the given criteria
     */
    List<AppointmentWithPatientDTO> getDoctorOwnAppointments(OffsetDateTime from, OffsetDateTime to);

    /**
     * Get a doctor
     *
     * @param id the id of the doctor
     * @return a doctor
     */
    DoctorDTO getDoctorById(UUID id);

    /**
     * Get all doctors
     *
     * @return a list of the doctors on the system
     */
    List<DoctorDTO> getDoctors();

    /**
     * Update the logged in doctor working hours
     * <p>
     * This does not affect appointments that are already booked
     *
     * @param doctorScheduleDTO the new schedule
     */
    void updateSchedule(DoctorScheduleDTO doctorScheduleDTO);
}
