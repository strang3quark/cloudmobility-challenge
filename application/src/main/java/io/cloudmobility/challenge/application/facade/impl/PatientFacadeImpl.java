package io.cloudmobility.challenge.application.facade.impl;

import io.cloudmobility.challenge.application.core.annotation.PatientAuthority;
import io.cloudmobility.challenge.application.core.exception.AppException;
import io.cloudmobility.challenge.application.core.exception.AppExceptionEnum;
import io.cloudmobility.challenge.application.facade.PatientFacade;
import io.cloudmobility.challenge.application.model.AccountEntity;
import io.cloudmobility.challenge.application.model.AppointmentEntity;
import io.cloudmobility.challenge.application.service.AccountContextService;
import io.cloudmobility.challenge.application.service.AppointmentService;
import io.cloudmobility.challenge.codegen.dto.AppointmentWithDoctorDTO;
import lombok.extern.log4j.Log4j2;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;

@Log4j2
@Component
public class PatientFacadeImpl implements PatientFacade {

    private final ModelMapper modelMapper;
    private final AccountContextService accountContextService;
    private final AppointmentService appointmentService;

    @Autowired
    public PatientFacadeImpl(ModelMapper modelMapper, AccountContextService accountContextService, AppointmentService appointmentService) {
        this.modelMapper = modelMapper;
        this.accountContextService = accountContextService;
        this.appointmentService = appointmentService;
    }

    @Override
    @PatientAuthority
    public void createAppointment(AppointmentWithDoctorDTO appointmentWithDoctorDTO) {
        var appointmentEntity = modelMapper.map(appointmentWithDoctorDTO, AppointmentEntity.class);
        appointmentEntity.setPatient(AccountEntity.builder().id(accountContextService.fetchUserId()).build());

        appointmentService.createAppointment(appointmentEntity);
    }

    @Override
    @PatientAuthority
    public void deleteAppointment(UUID id) {
        appointmentService.findAppointmentById(id)
                .filter(appointmentEntity -> Objects.nonNull(appointmentEntity.getPatient()))
                .filter(appointmentEntity ->
                        accountContextService.fetchUserId().equals(appointmentEntity.getPatient().getId())
                )
                .ifPresentOrElse(appointmentService::deleteAppointment,
                        () -> {
                            log.error("Cannot delete, Appointment does not exist: {}", id.toString());
                            throw new AppException(AppExceptionEnum.NOT_FOUND, String.format("Cannot delete %s", id), null);
                        });
    }

    @Override
    @PatientAuthority
    public List<AppointmentWithDoctorDTO> getPatientAppointments(OffsetDateTime from, OffsetDateTime to) {
        return appointmentService.findAppointments(
                null, accountContextService.fetchUserId(), from, to,
                null, null)
                .stream()
                .map(e -> modelMapper.map(e, AppointmentWithDoctorDTO.class))
                .collect(Collectors.toList());
    }
}
