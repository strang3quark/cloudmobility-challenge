package io.cloudmobility.challenge.application.service.impl;

import io.cloudmobility.challenge.application.core.exception.AppException;
import io.cloudmobility.challenge.application.core.exception.AppExceptionEnum;
import io.cloudmobility.challenge.application.model.AppointmentEntity;
import io.cloudmobility.challenge.application.repository.AppointmentRepository;
import io.cloudmobility.challenge.application.repository.specification.AppointmentSpecification;
import io.cloudmobility.challenge.application.service.AppointmentService;
import io.cloudmobility.challenge.application.service.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;

@Service
public class AppointmentServiceImpl implements AppointmentService {

    private final AppointmentRepository appointmentRepository;
    private final DoctorService doctorService;

    @Autowired
    public AppointmentServiceImpl(AppointmentRepository appointmentRepository, DoctorService doctorService) {
        this.appointmentRepository = appointmentRepository;
        this.doctorService = doctorService;
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<AppointmentEntity> findAppointmentById(UUID id) {
        return appointmentRepository.findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AppointmentEntity> findAppointments(
            UUID doctorId, UUID patientId,
            OffsetDateTime startDateFrom,
            OffsetDateTime startDateTo, OffsetDateTime endDateFrom, OffsetDateTime endDateTo) {

        final Specification<AppointmentEntity> specification =
                Specification.where(AppointmentSpecification.joinDoctor())
                        .and(AppointmentSpecification.joinPatient())
                        .and(doctorId == null ? null : AppointmentSpecification.filterDoctorId(doctorId))
                        .and(patientId == null ? null : AppointmentSpecification.filterPatientId(patientId))
                        .and(startDateFrom == null ? null :
                                AppointmentSpecification.filterScheduledDateTimeStartGreaterThan(startDateFrom))
                        .and(startDateTo == null ? null :
                                AppointmentSpecification.filterScheduledDateTimeStartLessThan(startDateTo))
                        .and(endDateFrom == null ? null :
                                AppointmentSpecification.filterScheduledDateTimeEndGreaterThan(endDateFrom))
                        .and(endDateTo == null ? null :
                                AppointmentSpecification.filterScheduledDateTimeEndLessThan(endDateTo));

        final var sort = Sort.by(Sort.Direction.ASC, AppointmentEntity.FIELD_DATETIME_START);

        return appointmentRepository.findAll(specification, sort);
    }

    @Override
    @Transactional
    public AppointmentEntity createAppointment(AppointmentEntity appointmentEntity) {

        // Doctors can mark unavailability in the past
        if (!ObjectUtils.nullSafeEquals(
                appointmentEntity.getDoctor().getId(),
                appointmentEntity.getPatient().getId())) {
            validateScheduleInThePast(appointmentEntity.getDatetimeStart());
        }

        validateCollision(
                appointmentEntity.getDoctor().getId(),
                appointmentEntity.getDatetimeStart(),
                appointmentEntity.getDatetimeEnd()
        );

        return appointmentRepository.save(appointmentEntity);
    }

    private void validateCollision(UUID doctorId, OffsetDateTime from, OffsetDateTime to) {
        validateCollisionWithDoctorSchedule(doctorId, from, to);
        validateCollisionWithOtherAppointments(doctorId, from, to);
    }

    private void validateScheduleInThePast(OffsetDateTime from) {
        if (from.isBefore(OffsetDateTime.now().minusHours(1))) {
            throw new AppException(AppExceptionEnum.ILLEGAL_OPERATION, "Cannot book events in the past", null);
        }
    }

    private void validateCollisionWithDoctorSchedule(UUID doctorId, OffsetDateTime from, OffsetDateTime to) {
        doctorService.findDoctorById(doctorId)
                .ifPresent(doctor -> {
                    final int appointmentHourStart = from.atZoneSameInstant(ZoneOffset.UTC).getHour();
                    final int appointmentHourEnd = to.atZoneSameInstant(ZoneOffset.UTC).getHour();

                    if (appointmentHourStart < doctor.getHourStart() || appointmentHourEnd > doctor.getHourEnd()) {
                        throwAlreadyBooked.accept(null);
                    }
                });
    }

    private void validateCollisionWithOtherAppointments(UUID doctorId, OffsetDateTime from, OffsetDateTime to) {
        final OffsetDateTime dayBegin = from.toLocalDate().atTime(0, 0).minusMinutes(1).atZone(ZoneOffset.UTC).toOffsetDateTime();
        final OffsetDateTime dayEnd = from.toLocalDate().atTime(23, 59).plusMinutes(1).atZone(ZoneOffset.UTC).toOffsetDateTime();

        findAppointments(doctorId, null, dayBegin, dayEnd, null, null)
                .stream()
                .filter(ap -> from.toEpochSecond() < ap.getDatetimeEnd().toEpochSecond())
                .filter(ap -> ap.getDatetimeStart().toEpochSecond() < to.toEpochSecond())
                .findAny().ifPresent(throwAlreadyBooked);
    }

    @Override
    @Transactional
    public void deleteAppointment(AppointmentEntity appointmentEntity) {
        appointmentRepository.delete(appointmentEntity);
    }

    private static final Consumer<Object> throwAlreadyBooked = p -> {
        throw new AppException(AppExceptionEnum.ALREADY_BOOKED, "This period is already booked", null);
    };
}
