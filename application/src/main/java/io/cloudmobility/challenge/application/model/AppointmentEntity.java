package io.cloudmobility.challenge.application.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;
import java.util.UUID;

@Entity
@Table(name = "appointment")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AppointmentEntity {

    public static final String FIELD_ID = "id";
    public static final String FIELD_DOCTOR = "doctor";
    public static final String FIELD_PATIENT = "patient";
    public static final String FIELD_DATETIME_START = "datetimeStart";
    public static final String FIELD_DATETIME_END = "datetimeEnd";

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id")
    private UUID id;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doctor_id")
    private DoctorEntity doctor;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "patient_id")
    private AccountEntity patient;

    @NotNull
    @Column(name = "datetime_start")
    private OffsetDateTime datetimeStart;

    @NotNull
    @Column(name = "datetime_end")
    private OffsetDateTime datetimeEnd;
}
