package io.cloudmobility.challenge.application.facade;

import io.cloudmobility.challenge.codegen.dto.AppointmentWithDoctorDTO;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

/**
 * Facade for Patient actions
 */
public interface PatientFacade {

    /**
     * Book an appointment for the logged in user with the specified doctor
     *
     * @param appointmentWithDoctorDTO the appointment information
     */
    void createAppointment(AppointmentWithDoctorDTO appointmentWithDoctorDTO);

    /**
     * Delete an appointment of the logged in user
     *
     * @param id the appointment id
     */
    void deleteAppointment(UUID id);

    /**
     * Get all appointments for the logged in doctor
     *
     * @param from filter appointments from this date on
     * @param to   filter appointments until this date
     *
     * @return a list with all the appointments of the logged in user (with doctor information)
     * that match the given criteria
     */
    List<AppointmentWithDoctorDTO> getPatientAppointments(OffsetDateTime from, OffsetDateTime to);
}
