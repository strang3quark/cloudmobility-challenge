package io.cloudmobility.challenge.application.repository.specification;

import io.cloudmobility.challenge.application.core.specification.BaseSpecification;
import io.cloudmobility.challenge.application.model.AccountEntity;
import io.cloudmobility.challenge.application.model.AppointmentEntity;
import io.cloudmobility.challenge.application.model.DoctorEntity;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.JoinType;
import java.time.OffsetDateTime;
import java.util.UUID;

/**
 * Specification for appointment queries
 */
public abstract class AppointmentSpecification extends BaseSpecification {

    private AppointmentSpecification() {

    }

    public static Specification<AppointmentEntity> joinDoctor() {
        return join(AppointmentEntity.FIELD_DOCTOR, JoinType.INNER);
    }

    public static Specification<AppointmentEntity> joinPatient() {
        return join(AppointmentEntity.FIELD_PATIENT, JoinType.INNER);
    }

    public static Specification<AppointmentEntity> filterDoctorId(UUID doctorId) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(
                root.get(AppointmentEntity.FIELD_DOCTOR).get(DoctorEntity.FIELD_ID), doctorId
        );
    }

    public static Specification<AppointmentEntity> filterPatientId(UUID patientId) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(
                root.get(AppointmentEntity.FIELD_PATIENT).get(AccountEntity.FIELD_ID), patientId
        );
    }

    public static Specification<AppointmentEntity> filterScheduledDateTimeStartLessThan(OffsetDateTime dateTimeLessThan) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(
                root.get(AppointmentEntity.FIELD_DATETIME_START), dateTimeLessThan
        );
    }

    public static Specification<AppointmentEntity> filterScheduledDateTimeStartGreaterThan(OffsetDateTime dateTimeGreaterThan) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(
                root.get(AppointmentEntity.FIELD_DATETIME_START), dateTimeGreaterThan
        );
    }

    public static Specification<AppointmentEntity> filterScheduledDateTimeEndLessThan(OffsetDateTime dateTimeLessThan) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.lessThanOrEqualTo(
                root.get(AppointmentEntity.FIELD_DATETIME_END), dateTimeLessThan
        );
    }

    public static Specification<AppointmentEntity> filterScheduledDateTimeEndGreaterThan(OffsetDateTime dateTimeGreaterThan) {
        return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.greaterThanOrEqualTo(
                root.get(AppointmentEntity.FIELD_DATETIME_END), dateTimeGreaterThan
        );
    }

    private static Specification<AppointmentEntity> join(String field, JoinType joinType) {
        return join(AppointmentEntity.class, field, joinType);
    }
}
