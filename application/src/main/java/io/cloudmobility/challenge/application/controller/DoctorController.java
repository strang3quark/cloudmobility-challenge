package io.cloudmobility.challenge.application.controller;

import io.cloudmobility.challenge.application.facade.DoctorFacade;
import io.cloudmobility.challenge.codegen.doctor.api.DoctorApi;
import io.cloudmobility.challenge.codegen.dto.AppointmentMinimalDTO;
import io.cloudmobility.challenge.codegen.dto.AppointmentWithPatientDTO;
import io.cloudmobility.challenge.codegen.dto.DoctorDTO;
import io.cloudmobility.challenge.codegen.dto.DoctorScheduleDTO;
import io.micrometer.core.annotation.Timed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import java.time.OffsetDateTime;
import java.util.List;
import java.util.UUID;

@Timed(value = "doctor-controller")
@RestController
public class DoctorController implements DoctorApi {

    private final DoctorFacade doctorFacade;

    @Autowired
    public DoctorController(DoctorFacade doctorFacade) {
        this.doctorFacade = doctorFacade;
    }

    @Override
    public ResponseEntity<Void> createUnavailable(AppointmentMinimalDTO appointmentMinimalDTO) {
        doctorFacade.createUnavailable(appointmentMinimalDTO);
        return ResponseEntity.status(HttpStatus.CREATED.value()).build();
    }

    @Override
    public ResponseEntity<Void> deleteUnavailable(UUID id) {
        doctorFacade.deleteUnavailable(id);
        return ResponseEntity.noContent().build();
    }

    @Override
    public ResponseEntity<List<AppointmentMinimalDTO>> getDoctorAppointments(UUID id, OffsetDateTime from, OffsetDateTime to) {
        final List<AppointmentMinimalDTO> resultBody = doctorFacade.getDoctorAppointments(id, from, to);
        return ResponseEntity.ok(resultBody);
    }

    @Override
    public ResponseEntity<List<AppointmentWithPatientDTO>> getDoctorOwnAppointments(OffsetDateTime from, OffsetDateTime to) {
        final List<AppointmentWithPatientDTO> resultBody = doctorFacade.getDoctorOwnAppointments(from, to);
        return ResponseEntity.ok(resultBody);
    }

    @Override
    public ResponseEntity<DoctorDTO> getDoctorById(UUID id) {
        final DoctorDTO doctorDTO = doctorFacade.getDoctorById(id);

        return ResponseEntity.ok(doctorDTO);
    }

    @Override
    public ResponseEntity<List<DoctorDTO>> getDoctors() {
        final List<DoctorDTO> doctorDTOList = doctorFacade.getDoctors();
        return ResponseEntity.ok(doctorDTOList);
    }

    @Override
    public ResponseEntity<Void> updateSchedule(DoctorScheduleDTO doctorScheduleDTO) {
        doctorFacade.updateSchedule(doctorScheduleDTO);
        return ResponseEntity.ok().build();
    }
}
