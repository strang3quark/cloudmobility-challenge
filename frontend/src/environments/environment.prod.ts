export const environment = {
  production: true,
  apiUrl: 'http://localhost:8080',
  oidcIssuer: 'http://localhost:9990/auth/realms/cloudmobility',
  oidcRequireHttps: false
};
