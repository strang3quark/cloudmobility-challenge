import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AlertService} from '../service/alert.service';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {OAuthService} from 'angular-oauth2-oidc';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(
    private oauthService: OAuthService,
    private alertService: AlertService,
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // Check if token is expired, if so, logout
    if (this.oauthService.getAccessToken() && this.oauthService.hasValidAccessToken() === false) {
      this.logout();
    }

    return next.handle(req)
      .pipe(
        // Logout on 401
        catchError((error: HttpErrorResponse, caught) => {
            if (error.status === 401) {
              this.logout();
              return next.handle(req);
            }
          }
        ));
  }


  logout(): void {
    this.alertService.emitAlert({
      global: true,
      type: 'warning',
      message: 'Your session has expired',
      showButton: true,
      buttonLabel: 'Ok',
      buttonAction: () => this.oauthService.logOut()
    });
  }

}
