import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import {EMPTY, Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {AlertService} from '../service/alert.service';
import {Alert} from '../model/alert.model';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {

  constructor(
    private alertService: AlertService,
  ) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request)
      .pipe(catchError((error: HttpErrorResponse, caught) => {

        if (!error || !error.error) {
          return next.handle(request);
        }

        const errorType = error.error.errorType ? error.error.errorType : 'Unknown';
        const extra = error.error.extra ? error.error.extra : error.error.message;

        const alert: Alert = {
          type: 'danger',
          message: errorType,
          extra,
          global: false
        };

        this.alertService.emitAlert(alert);

        return EMPTY;
      }));
  }
}
