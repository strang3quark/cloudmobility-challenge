import { TestBed } from '@angular/core/testing';

import { DoctorCalendarService } from './doctor-calendar.service';

describe('DoctorCalendarService', () => {
  let service: DoctorCalendarService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DoctorCalendarService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
