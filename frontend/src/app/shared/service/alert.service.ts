import {Injectable} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {Alert} from '../model/alert.model';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  private alert = new Subject<Alert>();

  constructor() {
  }

  get alert$(): Observable<Alert> {
    return this.alert.asObservable();
  }

  emitAlert(alert: Alert): void {
    this.alert.next(alert);
  }

  emitToast(message: string, type: 'danger' | 'warning' | 'info' | 'success' = 'success', timeout: number = 2000): void {
    this.emitAlert({
      type,
      global: false,
      message,
      timeout
    });
  }
}
