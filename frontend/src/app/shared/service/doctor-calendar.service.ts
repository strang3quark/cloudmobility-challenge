import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {DoctorScheduleDTO} from '../../../openapi/doctor';

@Injectable({
  providedIn: 'root'
})
export class DoctorCalendarService {

  private reloadCalendarSubject: Subject<void> = new Subject<void>();
  private updateShiftSubject: Subject<DoctorScheduleDTO> = new Subject<DoctorScheduleDTO>();

  public reloadCalendar$ = this.reloadCalendarSubject.asObservable();
  public updateShift$ = this.updateShiftSubject.asObservable();

  constructor() {
  }

  reloadCalendar(): void {
    this.reloadCalendarSubject.next();
  }

  updateShift(hourStart: number, hourEnd: number): void {
    this.updateShiftSubject.next({
      hourStart,
      hourEnd
    });
  }
}
