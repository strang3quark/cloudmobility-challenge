import {Injectable} from '@angular/core';
import {AuthConfig, NullValidationHandler, OAuthService} from 'angular-oauth2-oidc';
import {environment} from '../../../environments/environment';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  private authConfig: AuthConfig = {
    issuer: environment.oidcIssuer,
    redirectUri: window.location.origin + '/login',
    clientId: 'spa-appointment',
    scope: 'openid profile email offline_access',
    responseType: 'code',
    disableAtHashCheck: true,
    showDebugInformation: !environment.production,
    requireHttps: environment.oidcRequireHttps
  };

  constructor(
    private oauthService: OAuthService,
    private router: Router
  ) {
    this.configure();
  }

  public login(): void {
    this.oauthService.initLoginFlow();
  }

  public logout(): void {
    this.oauthService.logOut();
    this.router.navigate(['/']);
  }

  public isLoggedIn(): boolean {
    return this.oauthService.hasValidAccessToken();
  }

  public isDoctor(): boolean {
    return this.isLoggedIn() && this.getScopes().findIndex(scope => scope === 'doctor') > -1;
  }

  public isPatient(): boolean {
    return this.isLoggedIn() && this.getScopes().findIndex(scope => scope === 'patient') > -1;
  }

  public userId(): string {
    // @ts-ignore
    return this.oauthService.getIdentityClaims().sub;
  }

  public name(): string {
    // @ts-ignore
    return this.oauthService.getIdentityClaims().name;
  }

  private getScopes(): Array<string> {
    if (!this.isLoggedIn()) {
      return [];
    }
    const scopeString = this.oauthService.getGrantedScopes().toString();
    return scopeString.split(' ');
  }

  private configure(): void {
    this.oauthService.configure(this.authConfig);
    this.oauthService.tokenValidationHandler = new NullValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
  }
}
