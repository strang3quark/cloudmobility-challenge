import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TopbarComponent} from './component/topbar/topbar.component';
import {
  ClarityModule, ClrCheckboxModule, ClrCommonFormsModule, ClrDropdownModule,
  ClrFormsModule, ClrIconModule, ClrInputModule, ClrPasswordModule, ClrSelectModule
} from '@clr/angular';
import {DoctorCalendarComponent} from './component/doctor-calendar/doctor-calendar.component';
import {MessageboxComponent} from './component/messagebox/messagebox.component';
import {ReactiveFormsModule} from '@angular/forms';
import {AlertComponent} from './component/alert/alert.component';
import {GlobalAlertComponent} from './component/global-alert/global-alert.component';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {HttpErrorInterceptor} from './interceptor/http-error.interceptor';
import {AuthInterceptor} from './interceptor/auth.interceptor';
import {CalendarModule} from 'angular-calendar';
import {RouterModule} from '@angular/router';

const clarity = [
  ClarityModule,
  ClrFormsModule,
  ClrCommonFormsModule,
  ClrInputModule,
  ClrSelectModule,
  ClrPasswordModule,
  ClrCheckboxModule,
  ClrIconModule,
  ClrDropdownModule,
  ClrSelectModule,
];

const sharedComponents = [
  TopbarComponent,
  DoctorCalendarComponent,
  MessageboxComponent,
  GlobalAlertComponent,
  AlertComponent
];

@NgModule({
  declarations: [
    ...sharedComponents
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ...clarity,
    CalendarModule,
    RouterModule
  ],
  exports: [
    ReactiveFormsModule,
    ...sharedComponents,
    ...clarity,
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: HttpErrorInterceptor, multi: true}
  ]
})
export class SharedModule {
}
