import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-messagebox',
  templateUrl: './messagebox.component.html',
  styleUrls: ['./messagebox.component.scss']
})
export class MessageboxComponent implements OnInit {

  isOpen = true;
  @Input() title = '';
  @Input() text: Array<string> = [];
  @Input() showCancelButton = false;
  @Input() cancelButtonText = 'Cancel';
  @Input() okButtonText = 'Ok';
  @Output() messageBoxSubmit: EventEmitter<boolean> = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
  }

  onClick(ok: boolean): void {
    this.messageBoxSubmit.emit(ok);
  }
}
