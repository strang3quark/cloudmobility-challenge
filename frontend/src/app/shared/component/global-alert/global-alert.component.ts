import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {AlertService} from '../../service/alert.service';
import {filter} from 'rxjs/operators';
import {Alert} from '../../model/alert.model';

@Component({
  selector: 'app-global-alert',
  templateUrl: './global-alert.component.html',
  styleUrls: ['./global-alert.component.scss']
})
export class GlobalAlertComponent implements OnInit, OnDestroy {

  alert: Alert;

  private subscription: Subscription;

  constructor(
    private alertService: AlertService
  ) {}

  ngOnInit(): void {
    this.subscription = this.alertService.alert$
      .pipe(filter(v => v.global))
      .subscribe((alert) => {
      this.alert = alert;
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
