import {Component, OnDestroy, OnInit} from '@angular/core';
import {AlertService} from '../../service/alert.service';
import {Subscription} from 'rxjs';
import {filter} from 'rxjs/operators';
import {Alert} from '../../model/alert.model';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit, OnDestroy {

  alert: Alert;

  private subscription: Subscription;

  constructor(
    private alertService: AlertService
  ) {
  }

  ngOnInit(): void {
    this.subscription = this.alertService.alert$
      .pipe(filter(v => !v.global))
      .subscribe((alert) => {
        this.alert = alert;

        if (this.alert.timeout) {
          setTimeout(() => this.alert = undefined, this.alert.timeout);
        }
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
