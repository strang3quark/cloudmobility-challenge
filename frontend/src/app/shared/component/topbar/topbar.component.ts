import {Component, OnInit} from '@angular/core';
import {SessionService} from '../../service/session.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {

  constructor(
    private sessionService: SessionService
  ) {
  }

  ngOnInit(): void {
  }

  isLoggedIn(): boolean {
    return this.sessionService.isLoggedIn();
  }

  isDoctor(): boolean {
    return this.sessionService.isDoctor();
  }

  isPatient(): boolean {
    return this.sessionService.isPatient();
  }

  name(): string {
    return this.sessionService.name();
  }

  login(): void {
    this.sessionService.login();
  }

  logout(): void {
    this.sessionService.logout();
  }
}
