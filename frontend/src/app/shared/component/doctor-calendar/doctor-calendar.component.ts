import {Component, EventEmitter, Input, OnDestroy, OnInit, Output} from '@angular/core';
import {CalendarEvent, CalendarView} from 'angular-calendar';
import {parseISO, formatISO, startOfWeek, endOfWeek} from 'date-fns';
import {Subject} from 'rxjs';
import {AppointmentMinimalDTO, AppointmentWithPatientDTO, DoctorService} from '../../../../openapi/doctor';
import {DoctorDTO} from '../../../../openapi/patient';
import {SessionService} from '../../service/session.service';
import {takeUntil} from 'rxjs/operators';
import {DateUtils} from '../../utils/DateUtils';
import {DoctorCalendarService} from '../../service/doctor-calendar.service';
import {WeekDay} from "@angular/common";

@Component({
  selector: 'app-doctor-calendar',
  templateUrl: './doctor-calendar.component.html',
  styleUrls: ['./doctor-calendar.component.scss']
})
export class DoctorCalendarComponent implements OnInit, OnDestroy {

  view: CalendarView = CalendarView.Week;

  events: CalendarEvent[] = [];

  refreshCalendar: Subject<void> = new Subject<void>();

  @Input()
  viewDate: Date = new Date();

  @Input()
  selectedDoctor: DoctorDTO = undefined;

  @Output()
  eventClicked = new EventEmitter<CalendarEvent>();

  @Output()
  emptyClicked = new EventEmitter<Date>();

  @Output()
  dayClicked = new EventEmitter<Date>();

  hourStart: number;
  hourEnd: number;

  private destroy$ = new Subject<void>();

  constructor(
    private sessionService: SessionService,
    private doctorService: DoctorService,
    private doctorCalendarService: DoctorCalendarService
  ) {
  }

  ngOnInit(): void {

    this.changedDate(this.viewDate);

    this.doctorCalendarService.reloadCalendar$
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => this.changedDate(this.viewDate));

    this.doctorCalendarService.updateShift$
      .pipe(takeUntil(this.destroy$))
      .subscribe((shift) => this.updateShiftHours(shift.hourStart, shift.hourEnd));

    this.updateShiftHours(this.selectedDoctor.hourStart, this.selectedDoctor.hourEnd);
  }

  updateShiftHours(utcStart: number, utcEnd: number): void {
    this.hourStart = Number(DateUtils.utcToLocalHour(String(utcStart)));
    this.hourEnd = Number(DateUtils.utcToLocalHour(String(utcEnd)));
  }

  changedDate(date: Date): void {
    this.events = [];

    const start = startOfWeek(date);
    const end = endOfWeek(date);

    this.loadAppointments(formatISO(start), formatISO(end));
  }

  loadAppointments(from: string, to: string): void {
    if (this.isDoctor()) {
      this.doctorService.getDoctorOwnAppointments(from, to)
        .subscribe((appointments) => this.loadEventsIntoCalendar(appointments));
    } else {
      this.doctorService.getDoctorAppointments(this.selectedDoctor.id, from, to)
        .subscribe((appointments) => this.loadEventsIntoCalendar(appointments));
    }
  }

  loadEventsIntoCalendar(appointments: Array<AppointmentMinimalDTO | AppointmentWithPatientDTO>): void {
    appointments.forEach(appointment => {
        const isOwnAppointment = !('patient' in appointment) || appointment.patient.id === this.sessionService.userId();

        const beginDatetime = parseISO(appointment.datetimeStart.toString());
        const endDatetime = parseISO(appointment.datetimeEnd.toString());

        const event: CalendarEvent = {
          id: appointment.id,
          start: beginDatetime,
          end: endDatetime,
          title: isOwnAppointment ? '' : (appointment as AppointmentWithPatientDTO).patient.name,
          meta: {type: isOwnAppointment ? 'unavailable' : 'appointment', content: appointment},
          cssClass: isOwnAppointment ? 'unavailable-period' : undefined,
        };
        this.events.push(event);
        this.refreshCalendar.next();
      }
    );
  }

  handleEventClicked(event: CalendarEvent): void {
    this.eventClicked.emit(event);
  }

  handleSegmentClicked(event: { date: Date; sourceEvent: MouseEvent }): void {
    this.emptyClicked.emit(event.date);
  }

  handleDayClicked(event: { day: WeekDay; sourceEvent: MouseEvent }) {
    // @ts-ignore
    this.dayClicked.emit(event.day?.date);
  }

  isDoctor(): boolean {
    return this.sessionService.isDoctor();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
