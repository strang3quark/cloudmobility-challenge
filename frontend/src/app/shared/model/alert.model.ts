export class Alert {

  type: 'danger' | 'warning' | 'info' | 'success';
  message: string;
  extra?: string;
  showButton ? = false;
  buttonLabel?: string;
  buttonAction?: () => void;
  global = false;
  timeout?: number = undefined;


  constructor(type: 'danger' | 'warning' | 'info' | 'success', message: string) {
    this.type = type;
    this.message = message;
  }
}
