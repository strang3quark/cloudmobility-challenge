export interface EventMeta {
  type: string;
  content: object;
}
