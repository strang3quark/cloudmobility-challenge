import * as moment from 'moment';

export class DateUtils {

  static localToUTCHour(localHour: string): string {
    return moment(localHour, 'HH').utc().format('HH');
  }

  static utcToLocalHour(utcHour: string): string {
    const utcMoment = moment(utcHour, 'HH').format('HH');
    return moment.utc(utcMoment, 'HH').local().format('HH');
  }
}
