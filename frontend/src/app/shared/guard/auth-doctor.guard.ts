import {Injectable} from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  CanLoad,
  Route, Router,
  RouterStateSnapshot,
  UrlSegment,
  UrlTree
} from '@angular/router';
import {Observable} from 'rxjs';
import {SessionService} from '../service/session.service';

@Injectable({
  providedIn: 'root'
})
export class AuthDoctorGuard implements CanActivate, CanLoad {

  constructor(
    private sessionService: SessionService,
    private router: Router
  ) {
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    return this.confirmAuthElseRedirect();
  }

  canLoad(route: Route, segments: UrlSegment[]):
    Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    return this.confirmAuthElseRedirect();
  }

  private confirmAuthElseRedirect(): boolean{
    if (this.sessionService.isDoctor()) {
      return true;
    } else {
      this.router.navigateByUrl('/manager');
      return false;
    }
  }

}
