import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {DoctorAreaComponent} from './doctor-area.component';
import {DoctorOwnCalendarComponent} from './doctor-own-calendar/doctor-own-calendar.component';
import {DoctorSetWorkingHoursComponent} from './doctor-own-calendar/doctor-set-working-hours/doctor-set-working-hours.component';

const routes: Routes = [
  {
    path: '',
    component: DoctorAreaComponent,
    children: [
      {
        path: '',
        redirectTo: 'calendar'
      },
      {
        path: 'calendar',
        component: DoctorOwnCalendarComponent,
        children: [
          {
            path: 'shift',
            component: DoctorSetWorkingHoursComponent
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctorAreaRoutingModule {

}
