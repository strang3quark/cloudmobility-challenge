import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DoctorAreaComponent} from './doctor-area.component';
import {DoctorAreaRoutingModule} from './doctor-area-routing.module';
import { DoctorOwnCalendarComponent } from './doctor-own-calendar/doctor-own-calendar.component';
import {SharedModule} from '../../shared/shared.module';
import {CalendarModule} from 'angular-calendar';
import { DoctorCreateUnavailableComponent } from './doctor-own-calendar/doctor-create-unavailable/doctor-create-unavailable.component';
import { DoctorSetWorkingHoursComponent } from './doctor-own-calendar/doctor-set-working-hours/doctor-set-working-hours.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';



@NgModule({
  declarations: [
    DoctorAreaComponent,
    DoctorOwnCalendarComponent,
    DoctorCreateUnavailableComponent,
    DoctorSetWorkingHoursComponent
  ],
    imports: [
        CommonModule,
        DoctorAreaRoutingModule,
        SharedModule,
        CalendarModule,
        ReactiveFormsModule,
        FormsModule
    ]
})
export class DoctorAreaModule { }
