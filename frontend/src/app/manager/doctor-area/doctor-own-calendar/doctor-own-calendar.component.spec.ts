import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorOwnCalendarComponent } from './doctor-own-calendar.component';

describe('DoctorOwnCalendarComponent', () => {
  let component: DoctorOwnCalendarComponent;
  let fixture: ComponentFixture<DoctorOwnCalendarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoctorOwnCalendarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorOwnCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
