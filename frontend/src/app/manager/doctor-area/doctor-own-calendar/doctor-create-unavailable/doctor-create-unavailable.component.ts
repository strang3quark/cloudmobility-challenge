import {Component, Input, EventEmitter, OnInit, Output} from '@angular/core';
import {AppointmentMinimalDTO} from '../../../../../openapi/doctor';
import {addHours, addMinutes, formatISO} from 'date-fns';

@Component({
  selector: 'app-doctor-create-unavailable',
  templateUrl: './doctor-create-unavailable.component.html',
  styleUrls: ['./doctor-create-unavailable.component.scss']
})
export class DoctorCreateUnavailableComponent implements OnInit {

  isOpen = true;

  @Input()
  dateTime: Date;

  @Input()
  hourMin: number;

  @Input()
  hourMax: number;

  @Input()
  isWholeDay = false;

  @Input()
  editable: boolean = false;

  @Output()
  submitAppointment: EventEmitter<AppointmentMinimalDTO> = new EventEmitter<AppointmentMinimalDTO>();

  constructor() {
  }

  ngOnInit(): void {
  }

  submit(): void {
    const unavailable: AppointmentMinimalDTO = {};

    let startDate = new Date(this.dateTime);
    let endDate = new Date(this.dateTime);

    if (this.isWholeDay) {
      startDate.setHours(this.hourMin, 0, 0, 0);
      endDate.setHours(this.hourMax, 0, 0, 0);

      startDate = addMinutes(startDate, -startDate.getTimezoneOffset());
      endDate = addMinutes(endDate, -endDate.getTimezoneOffset());
    } else {
      endDate = addHours(new Date(this.dateTime), 1);
    }

    unavailable.datetimeStart = formatISO(startDate);
    unavailable.datetimeEnd = formatISO(endDate);

    this.submitAppointment.emit(unavailable);
  }

  exit(): void {
    this.submitAppointment.emit(undefined);
  }

  setWholeDay(wholeDay: boolean): void {
    this.isWholeDay = wholeDay;
  }

  dateFormat(): string {
    return this.isWholeDay ? 'yyyy-MM-dd' : 'yyyy-MM-dd HH:mm';
  }
}
