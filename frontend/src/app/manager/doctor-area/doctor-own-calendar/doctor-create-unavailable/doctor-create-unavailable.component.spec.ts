import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorCreateUnavailableComponent } from './doctor-create-unavailable.component';

describe('DoctorCreateUnavailableComponent', () => {
  let component: DoctorCreateUnavailableComponent;
  let fixture: ComponentFixture<DoctorCreateUnavailableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoctorCreateUnavailableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorCreateUnavailableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
