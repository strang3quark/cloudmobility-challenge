import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DoctorSetWorkingHoursComponent } from './doctor-set-working-hours.component';

describe('DoctorSetWorkingHoursComponent', () => {
  let component: DoctorSetWorkingHoursComponent;
  let fixture: ComponentFixture<DoctorSetWorkingHoursComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DoctorSetWorkingHoursComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DoctorSetWorkingHoursComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
