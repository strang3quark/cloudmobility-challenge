import {Component, OnInit} from '@angular/core';
import {SessionService} from '../../../../shared/service/session.service';
import {DoctorDTO, DoctorScheduleDTO, DoctorService} from '../../../../../openapi/doctor';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AlertService} from '../../../../shared/service/alert.service';
import {Router} from '@angular/router';
import {DateUtils} from '../../../../shared/utils/DateUtils';
import {DoctorCalendarService} from '../../../../shared/service/doctor-calendar.service';

@Component({
  selector: 'app-doctor-set-working-hours',
  templateUrl: './doctor-set-working-hours.component.html',
  styleUrls: ['./doctor-set-working-hours.component.scss']
})
export class DoctorSetWorkingHoursComponent implements OnInit {

  isOpen = true;
  doctor: DoctorDTO;

  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private sessionService: SessionService,
    private doctorService: DoctorService,
    private alertService: AlertService,
    private doctorCalendarService: DoctorCalendarService,
    private router: Router
  ) {
    this.form = fb.group({
      hourStart: ['', [Validators.required, Validators.min(0), Validators.max(23)]],
      hourEnd: ['', [Validators.required, Validators.min(0), Validators.max(23)]]
    });

    doctorService.getDoctorById(sessionService.userId())
      .subscribe((doctor) => {
        this.doctor = doctor;
        this.form.patchValue({
          hourStart: DateUtils.utcToLocalHour(String(doctor.hourStart)),
          hourEnd: DateUtils.utcToLocalHour(String(doctor.hourEnd))
        });
      });
  }

  ngOnInit(): void {
  }


  submit(): void{
    if (this.form.valid) {
      const schedule: DoctorScheduleDTO = {
        hourStart: Number(DateUtils.localToUTCHour(this.form.value.hourStart)),
        hourEnd: Number(DateUtils.localToUTCHour(this.form.value.hourEnd))
      };

      this.doctorService.updateSchedule(schedule).subscribe(() => {
        this.doctorCalendarService.updateShift(schedule.hourStart, schedule.hourEnd);

        this.alertService.emitToast('Working hours updated');
        this.router.navigateByUrl('/manager/doctor');
      });
    }
  }
}
