import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppointmentMinimalDTO, DoctorDTO, DoctorService} from '../../../../openapi/doctor';
import {CalendarEvent} from 'angular-calendar';
import {SessionService} from '../../../shared/service/session.service';
import {EventMeta} from '../../../shared/model/event-meta.model';
import {AlertService} from '../../../shared/service/alert.service';
import {DoctorCalendarService} from '../../../shared/service/doctor-calendar.service';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
  selector: 'app-doctor-own-calendar',
  templateUrl: './doctor-own-calendar.component.html',
  styleUrls: ['./doctor-own-calendar.component.scss']
})
export class DoctorOwnCalendarComponent implements OnInit, OnDestroy {

  selectedUnavailabilityPeriod: AppointmentMinimalDTO;
  selectedEmptySegment: Date = undefined;
  dayClickedElseHour: boolean;

  doctor: DoctorDTO;

  private destroy$ = new Subject<void>();

  constructor(
    private sessionService: SessionService,
    private doctorService: DoctorService,
    private alertService: AlertService,
    private doctorCalendarService: DoctorCalendarService
  ) {
    this.doctorService.getDoctorById(
      this.sessionService.userId()
    ).subscribe((doctor) => this.doctor = doctor);

    this.doctorCalendarService.updateShift$
      .pipe(takeUntil(this.destroy$))
      .subscribe((shift) => {
        this.doctor.hourStart = shift.hourStart;
        this.doctor.hourEnd = shift.hourEnd;
      });
  }

  ngOnInit(): void {
  }

  createUnavailabilityPeriod(appointmentMinimalDTO: AppointmentMinimalDTO): void {
    this.doctorService.createUnavailable(appointmentMinimalDTO).subscribe(() => {
      this.doctorCalendarService.reloadCalendar();
      this.selectedEmptySegment = undefined;

      this.alertService.emitToast('Record created');
    });
  }

  deleteUnavailabilityPeriod(): void {
    this.doctorService.deleteUnavailable(this.selectedUnavailabilityPeriod.id)
      .subscribe(() => {
        this.doctorCalendarService.reloadCalendar();
        this.selectedUnavailabilityPeriod = undefined;

        this.alertService.emitToast('Record deleted');
      });
  }

  handleEventClicked(event: CalendarEvent): void {
    const meta: EventMeta = event.meta;
    if (meta.type === 'unavailable') {
      this.selectedUnavailabilityPeriod = meta.content;
    }
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
