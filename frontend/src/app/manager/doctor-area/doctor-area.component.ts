import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-doctor-area',
  template: '<router-outlet></router-outlet>'
})
export class DoctorAreaComponent implements OnInit {

  constructor(
  ) {
  }

  ngOnInit(): void {
  }

}
