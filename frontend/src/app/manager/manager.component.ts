import {Component, OnInit} from '@angular/core';
import {SessionService} from '../shared/service/session.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-manager',
  template: '<router-outlet></router-outlet>'
})
export class ManagerComponent implements OnInit {

  constructor(
    private sessionService: SessionService,
    private router: Router
  ) {
    if (this.isDoctor()) {
      this.router.navigateByUrl('/manager/doctor');
    } else if (this.isPatient()) {
      this.router.navigateByUrl('/manager/patient');
    }
  }

  ngOnInit(): void {
  }

  isDoctor(): boolean {
    return this.sessionService.isDoctor();
  }

  isPatient(): boolean {
    return this.sessionService.isPatient();
  }

}
