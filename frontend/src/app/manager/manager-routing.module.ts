import {RouterModule, Routes} from '@angular/router';
import {ManagerComponent} from './manager.component';
import {NgModule} from '@angular/core';
import {AuthDoctorGuard} from '../shared/guard/auth-doctor.guard';
import {AuthPatientGuard} from '../shared/guard/auth-patient.guard';

const routes: Routes = [
  {
    path: '',
    component: ManagerComponent,
    pathMatch: 'full'
  },
  {
    path: 'doctor',
    loadChildren: () => import('./doctor-area/doctor-area.module').then(m => m.DoctorAreaModule),
    canLoad: [AuthDoctorGuard],
    canActivate: [AuthDoctorGuard]
  },
  {
    path: 'patient',
    loadChildren: () => import('./patient-area/patient-area.module').then(m => m.PatientAreaModule),
    canLoad: [AuthPatientGuard],
    canActivate: [AuthPatientGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManagerRoutingModule {

}
