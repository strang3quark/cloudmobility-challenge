import {Component, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {DoctorDTO, DoctorService} from '../../../../openapi/doctor';
import {AppointmentWithDoctorDTO, PatientService} from '../../../../openapi/patient';
import {addHours} from 'date-fns';
import {AlertService} from '../../../shared/service/alert.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-patient-create-appointment',
  templateUrl: './patient-create-appointment.component.html',
  styleUrls: ['./patient-create-appointment.component.scss']
})
export class PatientCreateAppointmentComponent implements OnInit {
  doctors$: Observable<DoctorDTO[]>;
  selectedDoctor: DoctorDTO;

  selectedTimeslot: Date;

  constructor(
    private doctorService: DoctorService,
    private patientService: PatientService,
    private alertService: AlertService,
    private router: Router
  ) {
    this.doctors$ = doctorService.getDoctors();
  }

  ngOnInit(): void {
  }

  handleEmptyClicked(date: Date): void {
    this.selectedTimeslot = date;
  }

  createAppointment(): void {
      const appointment: AppointmentWithDoctorDTO = {
        doctor: this.selectedDoctor,
        datetimeStart: this.selectedTimeslot.toISOString(),
        datetimeEnd: addHours(this.selectedTimeslot, 1).toISOString()
      };

      this.patientService.createAppointment(appointment).subscribe(
        () => {
          this.alertService.emitToast('Appointment booked');

          this.router.navigateByUrl('/manager/patient/appointments');
        }
      );
  }
}
