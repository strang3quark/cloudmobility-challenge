import {RouterModule, Routes} from '@angular/router';
import {PatientAreaComponent} from './patient-area.component';
import {PatientAppointmentsComponent} from './patient-appointments/patient-appointments.component';
import {NgModule} from '@angular/core';
import {PatientCreateAppointmentComponent} from './patient-create-appointment/patient-create-appointment.component';

const routes: Routes = [
  {
    path: '',
    component: PatientAreaComponent,
    children: [
      {
        path: '',
        redirectTo: 'appointments'
      },
      {
        path: 'appointments',
        component: PatientAppointmentsComponent
      },
      {
        path: 'new-appointment',
        component: PatientCreateAppointmentComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientAreaRoutingModule {

}
