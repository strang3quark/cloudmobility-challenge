import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-patient-area',
  template: '<router-outlet></router-outlet>'
})
export class PatientAreaComponent implements OnInit {

  constructor(
  ) {
  }

  ngOnInit(): void {
  }

}
