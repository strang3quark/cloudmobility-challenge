import {Component, OnInit} from '@angular/core';
import {AppointmentWithDoctorDTO, PatientService} from '../../../../openapi/patient';
import {AlertService} from '../../../shared/service/alert.service';

@Component({
  selector: 'app-patient-appointments',
  templateUrl: './patient-appointments.component.html',
  styleUrls: ['./patient-appointments.component.scss']
})
export class PatientAppointmentsComponent implements OnInit {

  appointments: AppointmentWithDoctorDTO[];
  selectedAppointment: AppointmentWithDoctorDTO;

  constructor(
    private patientService: PatientService,
    private alertService: AlertService
  ) {
    this.loadAppointments();
  }

  ngOnInit(): void {
  }

  deleteAppointment(): void {
    this.patientService.deleteAppointment(this.selectedAppointment.id)
      .subscribe(() => {
        this.alertService.emitToast('Appointment canceled');
        this.loadAppointments();
        this.selectedAppointment = undefined;
      });
  }

  loadAppointments(): void {
    this.patientService.getPatientAppointments(new Date().toISOString(), undefined)
      .subscribe((appointments) =>
        this.appointments = appointments
      );
  }
}
