import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PatientAreaComponent} from './patient-area.component';
import {PatientAppointmentsComponent} from './patient-appointments/patient-appointments.component';
import {SharedModule} from '../../shared/shared.module';
import {PatientAreaRoutingModule} from './patient-area.routing.module';
import {PatientCreateAppointmentComponent} from './patient-create-appointment/patient-create-appointment.component';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    PatientAreaComponent,
    PatientAppointmentsComponent,
    PatientCreateAppointmentComponent
  ],
  imports: [
    CommonModule,
    PatientAreaRoutingModule,
    SharedModule,
    FormsModule
  ]
})
export class PatientAreaModule {
}
