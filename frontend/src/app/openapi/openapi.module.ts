import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {OpenApiPatientModule} from './patient.configuration';
import {HttpClientModule} from '@angular/common/http';
import {OpenApiDoctorModule} from './doctor.configuration';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    HttpClientModule,
    OpenApiPatientModule,
    OpenApiDoctorModule
  ],
  exports: [
    HttpClientModule
  ]
})
export class OpenapiModule { }
