import {ApiModule, Configuration} from '../../openapi/patient';
import {environment} from '../../environments/environment';

function configFactory(): Configuration {
  return new Configuration({
    withCredentials: false,
    basePath: environment.apiUrl
  });
}

export const OpenApiPatientModule = ApiModule.forRoot(configFactory);
