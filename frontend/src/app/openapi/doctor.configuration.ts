import {ApiModule, Configuration} from '../../openapi/doctor';
import {environment} from '../../environments/environment';

function configFactory(): Configuration {
  return new Configuration({
    withCredentials: false,
    basePath: environment.apiUrl
  });
}

export const OpenApiDoctorModule = ApiModule.forRoot(configFactory);
