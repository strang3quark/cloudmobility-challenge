import {AfterViewInit, Component} from '@angular/core';
import {SessionService} from '../shared/service/session.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements AfterViewInit {

  constructor(
    private router: Router,
    private sessionService: SessionService
  ) {
  }

  ngAfterViewInit(): void {
    let attempt = 0;
    const maxAttempts = 3;

    const timer = setInterval(() => {

      if (this.sessionService.isLoggedIn()) {
        clearInterval(timer);
        this.router.navigateByUrl('/manager');
      }

      attempt += 1;

      if (attempt === maxAttempts) {
        clearInterval(timer);
        this.router.navigateByUrl('/');
      }
    }, 500);
  }
}
